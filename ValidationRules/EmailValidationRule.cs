﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace ProjekatPOP.ValidationRules
{
    class EmailValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            var email = value as string;
            if (email != null && email.Contains("@"))
            {
                return new ValidationResult(true, null);
            }
            return new ValidationResult(false, "Incorrect email format!");
        }

    }
}
