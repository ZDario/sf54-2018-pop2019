﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjekatPOP.NewException
{
    class InstitutionExistsException : Exception
    {
        public InstitutionExistsException(string message) : base(message)
        {

        }
    }
}
