﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjekatPOP.NewException
{
    class ClassroomExistsException : Exception
    {
        public ClassroomExistsException(string message) : base(message)
        {

        }
    }
}
