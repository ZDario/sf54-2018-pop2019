﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjekatPOP.NewException
{
    class UserExistsException : Exception
    {
        public UserExistsException(string message) : base(message)
        {

        }
    }
}
