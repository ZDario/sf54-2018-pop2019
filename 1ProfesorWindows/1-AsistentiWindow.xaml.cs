﻿using ProjekatPOP.DAO;
using ProjekatPOP.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjekatPOP
{
    /// <summary>
    /// Interaction logic for _1_AsistentiWindow.xaml
    /// </summary>
    public partial class _1_AsistentiWindow : Window
    {

        public ICollectionView CollectionView;

        public _1_AsistentiWindow()
        {
            InitializeComponent();
            CollectionView = CollectionViewSource.GetDefaultView(ModelHelper.GetInstance().Asistenti);
            CollectionView.Filter = ActiveFilter;
            DataContext = CollectionView;
        }

        public bool ActiveFilter(object obj)
        {

            var admin = obj as Admin;
            return admin.Active;
        }

        private void AddAsistentiButton_Click(object sender, RoutedEventArgs e)
        {
            Asistent noviAsistent = new Asistent();
            AddEditAsistentWindow addEditAsistentWindow = new AddEditAsistentWindow(noviAsistent);
            addEditAsistentWindow.ShowDialog();
            bool AsistentAdded = (bool)addEditAsistentWindow.DialogResult;
            if (AsistentAdded)
            {
                //korisnici.Add(addEditUserWindow.Korisnik);
                ModelHelper.GetInstance().AddAsistent(addEditAsistentWindow.Asistent);
                AsistentDAO.CreateAsistent(addEditAsistentWindow.Asistent);
            }
            CollectionView.Refresh();
        }

        private void DeleteAsistentiButton_Click(object sender, RoutedEventArgs e)
        {
            var institution = AsistentiGrid.SelectedItem as Asistent;
            if (institution == null)
            {
                return;
            }

            var result = MessageBox.Show("Jeste li sigurni da zelite da obrisete?",
                "Brisanje", MessageBoxButton.YesNo);
            if (result == MessageBoxResult.Yes)
            {
                var asistent = AsistentiGrid.SelectedItem as Asistent;
                ModelHelper.GetInstance().DeleteAsistent(asistent);
                //AsistentDAO.DeleteAsistent(asistent);
                CollectionView.Refresh();
            }
            else if (result == MessageBoxResult.No)
            {
                return;
            }
        }


        private void TerminButton_Click(object sender, RoutedEventArgs e)
        {
            var terminWindow = new TermWindow();
            terminWindow.Show();
        }

        private void RasporedButton_Click(object sender, RoutedEventArgs e)
        {
            var rasporedWindow = new _2_RasporedWindow();
            rasporedWindow.Show();
        }

        private void ProfilButton_Click(object sender, RoutedEventArgs e)
        {
            var profilWindow = new ProfilWindow();
            profilWindow.Show();
        }
    }
}
