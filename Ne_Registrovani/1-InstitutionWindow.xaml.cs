﻿using ProjekatPOP.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProjekatPOP.Ne_Registrovani
{
    /// <summary>
    /// Interaction logic for UstanovaWindow.xaml
    /// </summary>
    public partial class _1_InstitutionWindow : Window
    {

        public ICollectionView CollectionView;

        public _1_InstitutionWindow()
        {
            InitializeComponent();
            CollectionView = CollectionViewSource.GetDefaultView(ModelHelper.GetInstance().Ustanove);
            CollectionView.Filter = ActiveFilter;
            DataContext = CollectionView;
            //DataContext = korisnici;
            //Refresh();
        }

        public bool ActiveFilter(object obj)
        {

            var institution = obj as Ustanova;
            return institution.Active;
        }

        private void AsistantsButton_Click(object sender, RoutedEventArgs e)
        {
            var asistantWindow = new _2_AsistantWindow();
            asistantWindow.Show();
        }

        private void ProfesorsButton_Click(object sender, RoutedEventArgs e)
        {
            var profesorWindow = new _2_ProfesorWindow();
            profesorWindow.Show();
        }

        private void RasporedButton_Click(object sender, RoutedEventArgs e)
        {
            var rasporedWindow = new _2_RasporedWindow();
            rasporedWindow.Show();
        }
    }
}
