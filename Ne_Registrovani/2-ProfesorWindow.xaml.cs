﻿using ProjekatPOP.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProjekatPOP.Ne_Registrovani
{
    /// <summary>
    /// Interaction logic for _2_ProfesorWindow.xaml
    /// </summary>
    public partial class _2_ProfesorWindow : Window
    {

        public ICollectionView CollectionView;

        public _2_ProfesorWindow()
        {
            InitializeComponent();
            CollectionView = CollectionViewSource.GetDefaultView(ModelHelper.GetInstance().Profesori);
            CollectionView.Filter = ActiveFilter;
            DataContext = CollectionView;
            //DataContext = korisnici;
            //Refresh();
        }

        public bool ActiveFilter(object obj)
        {

            var institution = obj as Profesor;
            return institution.Active;
        }
    }
}
