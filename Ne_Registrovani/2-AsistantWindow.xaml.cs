﻿using ProjekatPOP.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProjekatPOP.Ne_Registrovani
{
    /// <summary>
    /// Interaction logic for _2_AsistantWindow.xaml
    /// </summary>
    public partial class _2_AsistantWindow : Window
    {
        public ICollectionView CollectionView;

        public _2_AsistantWindow()
        {
            InitializeComponent();
            CollectionView = CollectionViewSource.GetDefaultView(ModelHelper.GetInstance().Asistenti);
            CollectionView.Filter = ActiveFilter;
            DataContext = CollectionView;
            //DataContext = korisnici;
            //Refresh();
        }

        public bool ActiveFilter(object obj)
        {

            var institution = obj as Asistent;
            return institution.Active;
        }
    }
}
