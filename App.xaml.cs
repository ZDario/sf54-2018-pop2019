﻿using ProjekatPOP.DAO;
using ProjekatPOP.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;


namespace ProjekatPOP
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            //ModelHelper.GetInstance().InitAdmini();
            //ModelHelper.GetInstance().InitUstanove();
            //ModelHelper.GetInstance().InitUcionice();
            //ModelHelper.GetInstance().InitTermini();

            AdminDAO adminDAO = new AdminDAO();
            List<Admin> admini = adminDAO.SelectAdmin();
            ModelHelper.GetInstance().Admini = admini;

            AsistentDAO asistentDAO = new AsistentDAO();
            List<Asistent> asistenti = asistentDAO.SelectAsistent();
            ModelHelper.GetInstance().Asistenti = asistenti;

            ProfesorDAO profesorDAO = new ProfesorDAO();
            List<Profesor> profesori = profesorDAO.SelectProfesor();
            ModelHelper.GetInstance().Profesori = profesori;


            UstanovaDAO ustanovaDAO = new UstanovaDAO();
            List<Ustanova> ustanove = ustanovaDAO.SelectUstanova();
            ModelHelper.GetInstance().Ustanove = ustanove;

            UcionicaDAO ucionicaDAO = new UcionicaDAO();
            List<Ucionica> ucionice = ucionicaDAO.SelectUcionica();
            ModelHelper.GetInstance().Ucionice = ucionice;

            //TerminDAO terminDAO = new TerminDAO();
            //List<Termin> termini = terminDAO.SelectTermin();
            //ModelHelper.GetInstance().Termini = termini;


            //Ustanova ustanova1 = new Ustanova() { Naziv = "FTN", Lokacija = "Trg Dositeja Obradovica 6", Active = true };
            //UstanovaDAO ustanovaDAO = new UstanovaDAO();
            //ustanova1.Id = ustanovaDAO.CreateUstanova(ustanova1);
            //ustanova1.Naziv = "Neki drugi";
            //ustanovaDAO.UpdateUstanova(ustanova1);

            AdminWindow adminwindow = new AdminWindow();
            MainWindow window1 = new MainWindow();
            //InstitutionWindow window2 = new InstitutionWindow();
            //ClassroomWindow window3 = new ClassroomWindow();
            adminwindow.Show();
            window1.Show();
            //window2.Show();
            //window3.Show();
        }
    }
}
