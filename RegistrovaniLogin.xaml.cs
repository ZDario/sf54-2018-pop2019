﻿using ProjekatPOP.Model;
using ProjekatPOP;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProjekatPOP
{
    /// <summary>
    /// Interaction logic for AsistentLogin.xaml
    /// </summary>
    public partial class RegistrovaniLogin : Window
    {
        public RegistrovaniLogin()
        {
            InitializeComponent();
        }


        private void OkButton_Click(object sender, RoutedEventArgs e)
        {

            using (SqlConnection conn = new SqlConnection(ModelHelper.ConnectionString))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = ("SELECT * FROM Admin,Profesor,Asistent " +
                                        " WHERE Admin.KorisnickoIme='" + txtBoxUserName.Text.ToString().Trim() + "' AND Admin.Lozinka='" + txtBoxPassword.Password.ToString().Trim() + "' " +
                                        " OR Profesor.KorisnickoIme='" + txtBoxUserName.Text.ToString().Trim() + "' AND Profesor.Lozinka='" + txtBoxPassword.Password.ToString().Trim() + "' " +
                                        " OR Asistent.KorisnickoIme='" + txtBoxUserName.Text.ToString().Trim() + "' AND Asistent.Lozinka='" + txtBoxPassword.Password.ToString().Trim() + "' ");
                SqlDataAdapter da = new SqlDataAdapter(command);
                DataTable dt = new DataTable();
                da.Fill(dt);
                string cbItemValue = ComboBoxKorisnici.SelectedValue.ToString();
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i]["TipKorisnika"].ToString() == cbItemValue)
                        {
                            MessageBox.Show("You are logged in as " + dt.Rows[i]["TipKorisnika"]);
                            if (ComboBoxKorisnici.SelectedIndex == 0)
                            {
                                AdminWindow form = new AdminWindow();
                                form.Show();
                                this.Hide();
                            }
                            else if (ComboBoxKorisnici.SelectedIndex == 1)
                            {
                                _1_AsistentiWindow form = new _1_AsistentiWindow();
                                form.Show();
                                this.Hide();
                            }
                            else if (ComboBoxKorisnici.SelectedIndex == 2)
                            {
                                _1_TerminiWindow form = new _1_TerminiWindow();
                                form.Show();
                                this.Hide();
                            }
                            else if (ComboBoxKorisnici.SelectedIndex == null)
                            {
                                MessageBox.Show("Morate uneti tip korisnika.");
                            }
                            else if (txtBoxUserName.Equals(""))
                            {
                                MessageBox.Show("Morate uneti korisnicko ime.");
                            }
                            else if (txtBoxPassword.Equals(""))
                            {
                                MessageBox.Show("Morate uneti sifru.");
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Error!");
                }
            }

        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }
    }
}
