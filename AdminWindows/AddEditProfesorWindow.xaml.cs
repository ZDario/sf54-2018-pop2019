﻿using ProjekatPOP.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjekatPOP
{
    /// <summary>
    /// Interaction logic for AddEditProfesorWindow.xaml
    /// </summary>
    public partial class AddEditProfesorWindow : Window
    {
        public Profesor Profesor { get; set; }

        public enum ProfesorWindowState { Addition, Edition };

        private ProfesorWindowState state;

        public AddEditProfesorWindow(Profesor profesor, ProfesorWindowState profesorWindowState = ProfesorWindowState.Addition)
        {
            InitializeComponent();

            Profesor = profesor;
            state = profesorWindowState;

            RolesComboBox.Items.Add("Profesor");

            DataContext = Profesor;
        }

        private void SaveProfesorButton_Click(object sender, RoutedEventArgs e)
        {
            if (!(state == ProfesorWindowState.Edition && String.IsNullOrWhiteSpace(ProfesorPassword.Password)))
            {
                Profesor.Lozinka = ProfesorPassword.Password;
            }

            Profesor.Lozinka = ProfesorPassword.Password;

            string selectedRole = (string)RolesComboBox.SelectedItem;
            if (selectedRole != null)
            {
                enumTipKorisnika tipKorisnika;
                Enum.TryParse(selectedRole, out tipKorisnika);

                Profesor.TipKorisnika = tipKorisnika;


                DialogResult = true;
                Close();
            }
        }

        private void DeleteProfesorButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }
    }
}
