﻿using ProjekatPOP.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProjekatPOP
{
    /// <summary>
    /// Interaction logic for AddEditTermWindow.xaml
    /// </summary>
    public partial class AddEditTermWindow : Window
    {
        public Termin Termin { get; set; }

        public enum TermWindowState { Addition, Edition };

        private TermWindowState state;

        public AddEditTermWindow(Termin termin, TermWindowState termWindowState = TermWindowState.Addition)
        {
            InitializeComponent();

            Termin = termin;
            state = termWindowState;

            DayInWeekComboBox.Items.Add("Ponedeljak");
            DayInWeekComboBox.Items.Add("Utorak");
            DayInWeekComboBox.Items.Add("Sreda");
            DayInWeekComboBox.Items.Add("Cetvrtak");
            DayInWeekComboBox.Items.Add("Petak");
            DayInWeekComboBox.Items.Add("Subota");
            DayInWeekComboBox.Items.Add("Nedelja");

            TypeOfTeachingComboBox.Items.Add("Predavanje");
            TypeOfTeachingComboBox.Items.Add("Auditorne_Vezbe");
            TypeOfTeachingComboBox.Items.Add("Racunarske_Vezbe");
            TypeOfTeachingComboBox.Items.Add("Konsultacije");

            DataContext = Termin;
        }

        private void SaveTermButton_Click(object sender, RoutedEventArgs e)
        {
            string selectedDayInWeek = (string)DayInWeekComboBox.SelectedItem;
            if (selectedDayInWeek != null)
            {
                enumDaniUNedelji daniUNedelji;
                Enum.TryParse(selectedDayInWeek, out daniUNedelji);

                Termin.DaniUNedelji = daniUNedelji;
            }

            string selectedTypeOfTeaching = (string)TypeOfTeachingComboBox.SelectedItem;
            if (selectedTypeOfTeaching != null)
            {
                enumTipNastave tipNastave;
                Enum.TryParse(selectedTypeOfTeaching, out tipNastave);

                Termin.TipNastave = tipNastave;


                
            }
            DialogResult = true;
            Close();
        }

        private void DeleteTermButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }
    }
}
