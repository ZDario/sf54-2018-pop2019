﻿using ProjekatPOP.DAO;
using ProjekatPOP.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProjekatPOP
{
    /// <summary>
    /// Interaction logic for TerminWindow.xaml
    /// </summary>
    public partial class TermWindow : Window
    {

        public ICollectionView CollectionView;

        public TermWindow()
        {
            InitializeComponent();
            CollectionView = CollectionViewSource.GetDefaultView(ModelHelper.GetInstance().Termini);
            CollectionView.Filter = ActiveFilter;
            DataContext = CollectionView;
        }

        public bool ActiveFilter(object obj)
        {

            var term = obj as Termin;
            return term.Active;
        }

        private void AddTermButton_Click(object sender, RoutedEventArgs e)
        {
            Termin noviTermin = new Termin();
            AddEditTermWindow addEditTermWindow = new AddEditTermWindow(noviTermin);
            addEditTermWindow.ShowDialog();
            bool termAdded = (bool)addEditTermWindow.DialogResult;
            if (termAdded)
            {
                //korisnici.Add(addEditUserWindow.Korisnik);
                ModelHelper.GetInstance().AddTermin(addEditTermWindow.Termin);
                TerminDAO.CreateTermin(addEditTermWindow.Termin);
            }
            CollectionView.Refresh();
        }

        private void EditTermButton_Click(object sender, RoutedEventArgs e)
        {
            var term = TermsGrid.SelectedItem as Termin;
            if (term == null)
            {
                return;
            }

            var termCopy = new Termin(term);
            var editTermWindow = new AddEditTermWindow(termCopy, AddEditTermWindow.TermWindowState.Edition);
            editTermWindow.ShowDialog();

            bool shouldEditTerm = editTermWindow.DialogResult.Value;
            if (shouldEditTerm)
            {
                ModelHelper.GetInstance().UpdateTermin(term, termCopy);
                TerminDAO terminDAO = new TerminDAO();
                terminDAO.UpdateTermin(termCopy);

                //var index = korisnici.IndexOf(user);
                //korisnici[index] = userCopy;
            }
            CollectionView.Refresh();
        }

        private void DeleteTermButton_Click(object sender, RoutedEventArgs e)
        {
            var result = MessageBox.Show("Jeste li sigurni da zelite da obrisete?",
                "Brisanje", MessageBoxButton.YesNo);
            if (result == MessageBoxResult.Yes)
            {
                var termin = TermsGrid.SelectedItem as Termin;
                ModelHelper.GetInstance().DeleteTermin(termin);
                TerminDAO.DeleteTermin(termin);
                CollectionView.Refresh();
            }
            else if (result == MessageBoxResult.No)
            {
                return;
            }
        }
    }
}
