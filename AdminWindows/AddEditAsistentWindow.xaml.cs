﻿using ProjekatPOP.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjekatPOP
{
    /// <summary>
    /// Interaction logic for AddEditAsistentWindow.xaml
    /// </summary>
    public partial class AddEditAsistentWindow : Window
    {
        public Asistent Asistent { get; set; }

        public enum AsistentWindowState { Addition, Edition };

        private AsistentWindowState state;

        public AddEditAsistentWindow(Asistent asistent, AsistentWindowState asistentWindowState = AsistentWindowState.Addition)
        {
            InitializeComponent();

            Asistent = asistent;
            state = asistentWindowState;

            RolesComboBox.Items.Add("Asistent");

            DataContext = Asistent;
        }

        private void SaveAsistentButton_Click(object sender, RoutedEventArgs e)
        {
            if (!(state == AsistentWindowState.Edition && String.IsNullOrWhiteSpace(AsistentPassword.Password)))
            {
                Asistent.Lozinka = AsistentPassword.Password;
            }

            Asistent.Lozinka = AsistentPassword.Password;

            string selectedRole = (string)RolesComboBox.SelectedItem;
            if (selectedRole != null)
            {
                enumTipKorisnika tipKorisnika;
                Enum.TryParse(selectedRole, out tipKorisnika);

                Asistent.TipKorisnika = tipKorisnika;


                DialogResult = true;
                Close();
            }
        }

        private void DeleteAsistentButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }
    }
}
