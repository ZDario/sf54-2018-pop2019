﻿using ProjekatPOP.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProjekatPOP
{
    /// <summary>
    /// Interaction logic for AddEditInstitutionWindow.xaml
    /// </summary>
    public partial class AddEditInstitutionWindow : Window
    {
        public Ustanova Ustanova { get; set; }

        public enum InstitutionWindowState { Addition, Edition };

        private InstitutionWindowState state;

        public AddEditInstitutionWindow(Ustanova ustanova, InstitutionWindowState institutionWindowState = InstitutionWindowState.Addition)
        {
            InitializeComponent();

            Ustanova = ustanova;
            state = institutionWindowState;

            DataContext = Ustanova;
        }

        private void SaveInstitutionButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        private void DeleteInstitutionButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }
    }
}
