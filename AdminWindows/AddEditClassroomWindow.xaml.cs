﻿using ProjekatPOP.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProjekatPOP
{
    /// <summary>
    /// Interaction logic for AddEditClassroomWindow.xaml
    /// </summary>
    public partial class AddEditClassroomWindow : Window
    {
        public Ucionica Ucionica { get; set; }

        public enum ClassroomWindowState { Addition, Edition };

        private ClassroomWindowState state;

        public AddEditClassroomWindow(Ucionica ucionica, ClassroomWindowState classroomWindowState = ClassroomWindowState.Addition)
        {
            InitializeComponent();

            Ucionica = ucionica;
            state = classroomWindowState;

            RolesComboBox.Items.Add("Sa_Racunarima");
            RolesComboBox.Items.Add("Amfiteatar");
            RolesComboBox.Items.Add("Normalna_Ucionica");
            RolesComboBox.Items.Add("Veca_Ucionica");

            DataContext = Ucionica;
        }

        private void SaveClassroomButton_Click(object sender, RoutedEventArgs e)
        {
            string selectedRole = (string)RolesComboBox.SelectedItem;
            if(selectedRole != null)
            {
                enumTipUcionice tipUcionice;
                Enum.TryParse(selectedRole, out tipUcionice);

                Ucionica.TipUcionice = tipUcionice;


                DialogResult = true;
                Close();
            }
           
        }

        private void DeleteClassroomButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }
    }
}
