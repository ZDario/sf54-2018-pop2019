﻿using ProjekatPOP.DAO;
using ProjekatPOP.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProjekatPOP
{
    /// <summary>
    /// Interaction logic for UstanovaWindow.xaml
    /// </summary>
    public partial class InstitutionWindow : Window
    {

        public ICollectionView CollectionView;

        public InstitutionWindow()
        {
            InitializeComponent();
            CollectionView = CollectionViewSource.GetDefaultView(ModelHelper.GetInstance().Ustanove);
            CollectionView.Filter = ActiveFilter;
            DataContext = CollectionView;
            //Refresh();
        }

        public bool ActiveFilter(object obj)
        {

            var institution = obj as Ustanova;
            return institution.Active;
        }

        private void AddInstitutionButton_Click(object sender, RoutedEventArgs e)
        {
            Ustanova novaUstanova = new Ustanova();
            AddEditInstitutionWindow addEditInstitutionWindow = new AddEditInstitutionWindow(novaUstanova);
            addEditInstitutionWindow.ShowDialog();
            bool institutionAdded = (bool)addEditInstitutionWindow.DialogResult;
            if (institutionAdded)
            {
                //korisnici.Add(addEditUserWindow.Korisnik);
                ModelHelper.GetInstance().AddUstanova(addEditInstitutionWindow.Ustanova);
                UstanovaDAO.CreateUstanova(addEditInstitutionWindow.Ustanova);
            }
            CollectionView.Refresh();
        }

        private void EditInstitutionButton_Click(object sender, RoutedEventArgs e)
        {
            var institution = InstitutionsGrid.SelectedItem as Ustanova;
            if (institution == null)
            {
                return;
            }

            var institutionCopy = new Ustanova(institution);
            var editInstitutionWindow = new AddEditInstitutionWindow(institutionCopy, AddEditInstitutionWindow.InstitutionWindowState.Edition);
            editInstitutionWindow.ShowDialog();

            bool shouldEditInstitution = editInstitutionWindow.DialogResult.Value;
            if (shouldEditInstitution)
            {
                ModelHelper.GetInstance().UpdateUstanova(institution, institutionCopy);
                UstanovaDAO ustanovaDAO = new UstanovaDAO();
                ustanovaDAO.UpdateUstanova(institutionCopy);

                //var index = korisnici.IndexOf(user);
                //korisnici[index] = userCopy;
            }
            CollectionView.Refresh();
        }

        private void DeleteInstitutionButton_Click(object sender, RoutedEventArgs e)
        {
            var institution = InstitutionsGrid.SelectedItem as Ustanova;
            if (institution == null)
            {
                return;
            }

            var result = MessageBox.Show("Jeste li sigurni da zelite da obrisete?",
                "Brisanje", MessageBoxButton.YesNo);
            if (result == MessageBoxResult.Yes)
            {
                var ustanova = InstitutionsGrid.SelectedItem as Ustanova;
                ModelHelper.GetInstance().DeleteUstanova(ustanova);
                UstanovaDAO.DeleteUstanova(ustanova);
                CollectionView.Refresh();
            }
            else if (result == MessageBoxResult.No)
            {
                return;
            }
        }

        private void ClassroomsButton_Click(object sender, RoutedEventArgs e)
        {
            var institution = InstitutionsGrid.SelectedItem as Ustanova;
            if (institution == null)
            {
                return;
            }

            var classroomWindow = new ClassroomWindow();
            classroomWindow.Show();
        }

        private void TermsButton_Click(object sender, RoutedEventArgs e)
        {
            var termWindow = new TermWindow();
            termWindow.Show();
        }

        private void AsistentsButton_Click(object sender, RoutedEventArgs e)
        {
            var asistentWindow = new AsistentWindow();
            asistentWindow.Show();
        }

        private void ProfesorsButton_Click(object sender, RoutedEventArgs e)
        {
            var profesorWindow = new ProfesorWindow();
            profesorWindow.Show();
        }
    }
}
