﻿using ProjekatPOP.DAO;
using ProjekatPOP.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjekatPOP
{
    /// <summary>
    /// Interaction logic for AsistentWindow.xaml
    /// </summary>
    public partial class AsistentWindow : Window
    {
        public ICollectionView CollectionView;

        public AsistentWindow()
        {
            InitializeComponent();
            CollectionView = CollectionViewSource.GetDefaultView(ModelHelper.GetInstance().Asistenti);
            CollectionView.Filter = ActiveFilter;
            DataContext = CollectionView;
            //DataContext = korisnici;
            //Refresh();
        }

        public bool ActiveFilter(object obj)
        {

            var asistent = obj as Asistent;
            return asistent.Active;
        }

        //private void Refresh()
        //{
        //    UsersGrid.ItemsSource = null;
        //    UsersGrid.ItemsSource = ModelHelper.GetInstance().Korisnici;
        //}

        private void AddAsistentButton_Click(object sender, RoutedEventArgs e)
        {
            Asistent noviAsistent = new Asistent();
            AddEditAsistentWindow addEditAsistentWindow = new AddEditAsistentWindow(noviAsistent);
            addEditAsistentWindow.ShowDialog();
            bool AsistentAdded = (bool)addEditAsistentWindow.DialogResult;
            if (AsistentAdded)
            {
                //korisnici.Add(addEditUserWindow.Korisnik);
                ModelHelper.GetInstance().AddAsistent(addEditAsistentWindow.Asistent);
                AsistentDAO.CreateAsistent(addEditAsistentWindow.Asistent);
            }
            CollectionView.Refresh();
        }

        private void EditAsistentButton_Click(object sender, RoutedEventArgs e)
        {
            var asistent = AsistentsGrid.SelectedItem as Asistent;
            if (asistent == null)
            {
                return;
            }

            var asistentCopy = new Asistent(asistent);
            var editAsistentWindow = new AddEditAsistentWindow(asistentCopy, AddEditAsistentWindow.AsistentWindowState.Edition);
            editAsistentWindow.ShowDialog();

            bool shouldEditAsistent = editAsistentWindow.DialogResult.Value;
            if (shouldEditAsistent)
            {
                ModelHelper.GetInstance().UpdateAsistent(asistent, asistentCopy);
                AsistentDAO asistentDAO = new AsistentDAO();
                asistentDAO.UpdateAsistent(asistentCopy);

                //var index = korisnici.IndexOf(user);
                //korisnici[index] = userCopy;
            }
            CollectionView.Refresh();
        }

        private void DeleteAsistentButton_Click(object sender, RoutedEventArgs e)
        {
            var institution = AsistentsGrid.SelectedItem as Asistent;
            if (institution == null)
            {
                return;
            }

            var result = MessageBox.Show("Jeste li sigurni da zelite da obrisete?",
                "Brisanje", MessageBoxButton.YesNo);
            if (result == MessageBoxResult.Yes)
            {
                var asistent = AsistentsGrid.SelectedItem as Asistent;
                ModelHelper.GetInstance().DeleteAsistent(asistent);
                AsistentDAO.DeleteAsistent(asistent);
                CollectionView.Refresh();
            }
            else if (result == MessageBoxResult.No)
            {
                return;
            }
        }

        private void InstitutionsButton_Click(object sender, RoutedEventArgs e)
        {
            var institutionWindow = new InstitutionWindow();
            institutionWindow.Show();
        }
    }
}
