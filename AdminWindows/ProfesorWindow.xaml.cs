﻿using ProjekatPOP.DAO;
using ProjekatPOP.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProjekatPOP
{
    /// <summary>
    /// Interaction logic for ProfesorWindow.xaml
    /// </summary>
    public partial class ProfesorWindow : Window
    {
        public ICollectionView CollectionView;

        public ProfesorWindow()
        {
            InitializeComponent();
            CollectionView = CollectionViewSource.GetDefaultView(ModelHelper.GetInstance().Profesori);
            CollectionView.Filter = ActiveFilter;
            DataContext = CollectionView;
        }

        public bool ActiveFilter(object obj)
        {

            var profesor = obj as Profesor;
            return profesor.Active;
        }

        private void AddProfesorButton_Click(object sender, RoutedEventArgs e)
        {
            Profesor noviProfesor = new Profesor();
            AddEditProfesorWindow addEditProfesorWindow = new AddEditProfesorWindow(noviProfesor);
            addEditProfesorWindow.ShowDialog();
            bool profesorAdded = (bool)addEditProfesorWindow.DialogResult;
            if (profesorAdded)
            {
                //korisnici.Add(addEditUserWindow.Korisnik);
                ModelHelper.GetInstance().AddProfesor(addEditProfesorWindow.Profesor);
                ProfesorDAO.CreateProfesor(addEditProfesorWindow.Profesor);
            }
            CollectionView.Refresh();
        }

        private void EditProfesorButton_Click(object sender, RoutedEventArgs e)
        {
            var profesor = ProfesorsGrid.SelectedItem as Profesor;
            if (profesor == null)
            {
                return;
            }

            var profesorCopy = new Profesor(profesor);
            var editProfesorWindow = new AddEditProfesorWindow(profesorCopy, AddEditProfesorWindow.ProfesorWindowState.Edition);
            editProfesorWindow.ShowDialog();

            bool shouldEditProfesor = editProfesorWindow.DialogResult.Value;
            if (shouldEditProfesor)
            {
                ModelHelper.GetInstance().UpdateProfesor(profesor, profesorCopy);
                ProfesorDAO profesorDAO = new ProfesorDAO();
                profesorDAO.UpdateProfesor(profesorCopy);

                //var index = korisnici.IndexOf(user);
                //korisnici[index] = userCopy;
            }
            CollectionView.Refresh();
        }

        private void DeleteProfesorButton_Click(object sender, RoutedEventArgs e)
        {
            var institution = ProfesorsGrid.SelectedItem as Profesor;
            if (institution == null)
            {
                return;
            }

            var result = MessageBox.Show("Jeste li sigurni da zelite da obrisete?",
                "Brisanje", MessageBoxButton.YesNo);
            if (result == MessageBoxResult.Yes)
            {
                var profesor = ProfesorsGrid.SelectedItem as Profesor;
                ModelHelper.GetInstance().DeleteProfesor(profesor);
                ProfesorDAO.DeleteProfesor(profesor);
                CollectionView.Refresh();
            }
            else if (result == MessageBoxResult.No)
            {
                return;
            }
        }
    }
}
