﻿using ProjekatPOP.DAO;
using ProjekatPOP.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProjekatPOP
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class AdminWindow : Window
    {

        //ObservableCollection<Korisnik> korisnici = new ObservableCollection<Korisnik>(ModelHelper.GetInstance().Korisnici);

        public ICollectionView CollectionView;

        public AdminWindow()
        {
            InitializeComponent();
            CollectionView = CollectionViewSource.GetDefaultView(ModelHelper.GetInstance().Admini);
            CollectionView.Filter = ActiveFilter;
            DataContext = CollectionView;
            //DataContext = korisnici;
            //Refresh();
        }

        public bool ActiveFilter(object obj)
        {

            var admin = obj as Admin;
            return admin.Active;
        }

        //private void Refresh()
        //{
        //    UsersGrid.ItemsSource = null;
        //    UsersGrid.ItemsSource = ModelHelper.GetInstance().Korisnici;
        //}

        private void AddAdminButton_Click(object sender, RoutedEventArgs e)
        {
            Admin noviAdmin = new Admin();
            AddEditAdminWindow addEditAdminWindow = new AddEditAdminWindow(noviAdmin);
            addEditAdminWindow.ShowDialog();
            bool adminAdded = (bool)addEditAdminWindow.DialogResult;
            if (adminAdded)
            {
                //korisnici.Add(addEditUserWindow.Korisnik);
                ModelHelper.GetInstance().AddAdmin(addEditAdminWindow.Admin);
                AdminDAO.CreateAdmin(addEditAdminWindow.Admin);
            }
            CollectionView.Refresh();
        }

        private void EditAdminButton_Click(object sender, RoutedEventArgs e)
        {
            var administrator = AdminsGrid.SelectedItem as Admin;
            if (administrator == null)
            {
                return;
            }

            var adminCopy = new Admin(administrator);
            var editAdminWindow = new AddEditAdminWindow(adminCopy, AddEditAdminWindow.AdminWindowState.Edition);
            editAdminWindow.ShowDialog();

            bool shouldEditAdmin = editAdminWindow.DialogResult.Value;
            if (shouldEditAdmin)
            {
                ModelHelper.GetInstance().UpdateAdmin(administrator, adminCopy);
                AdminDAO adminDAO = new AdminDAO();
                adminDAO.UpdateAdmin(adminCopy);

                //var index = korisnici.IndexOf(user);
                //korisnici[index] = userCopy;
            }
            CollectionView.Refresh();
        }

        private void DeleteAdminButton_Click(object sender, RoutedEventArgs e)
        {
            var administrator = AdminsGrid.SelectedItem as Admin;
            if (administrator == null)
            {
                return;
            }

            var result = MessageBox.Show("Jeste li sigurni da zelite da obrisete?",
                "Brisanje", MessageBoxButton.YesNo);
            if(result == MessageBoxResult.Yes)
            {
                var admin = AdminsGrid.SelectedItem as Admin;
                ModelHelper.GetInstance().DeleteAdmin(admin);
                AdminDAO.DeleteAdmin(admin);
                CollectionView.Refresh();
            }
            else if(result == MessageBoxResult.No)
            {
                return;
            }
        }

        private void InstitutionsButton_Click(object sender, RoutedEventArgs e)
        {
            var institutionWindow = new InstitutionWindow();
            institutionWindow.Show();
        }
    }
}
