﻿using ProjekatPOP.DAO;
using ProjekatPOP.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProjekatPOP
{
    /// <summary>
    /// Interaction logic for ClassroomWindow.xaml
    /// </summary>
    public partial class ClassroomWindow : Window
    {

        public ICollectionView CollectionView;

        public ClassroomWindow()
        {
            InitializeComponent();
            CollectionView = CollectionViewSource.GetDefaultView(ModelHelper.GetInstance().Ucionice);
            CollectionView.Filter = ActiveFilter;
            DataContext = CollectionView;
            //DataContext = korisnici;
            //Refresh();
        }

        public bool ActiveFilter(object obj)
        {

            var classroom = obj as Ucionica;
            return classroom.Active;
        }

        private void AddClassroomButton_Click(object sender, RoutedEventArgs e)
        {
            Ucionica novaUcionica = new Ucionica();
            AddEditClassroomWindow addEditClassroomWindow = new AddEditClassroomWindow(novaUcionica);
            addEditClassroomWindow.ShowDialog();
            bool classroomAdded = (bool)addEditClassroomWindow.DialogResult;
            if (classroomAdded)
            {
                //korisnici.Add(addEditUserWindow.Korisnik);
                ModelHelper.GetInstance().AddUcionica(addEditClassroomWindow.Ucionica);
                UcionicaDAO.CreateUcionica(addEditClassroomWindow.Ucionica);

            }
            CollectionView.Refresh();
        }

        private void EditClassroomButton_Click(object sender, RoutedEventArgs e)
        {
            var classroom = ClassroomsGrid.SelectedItem as Ucionica;
            if (classroom == null)
            {
                return;
            }

            var classroomCopy = new Ucionica(classroom);
            var editClassroomWindow = new AddEditClassroomWindow(classroomCopy, AddEditClassroomWindow.ClassroomWindowState.Edition);
            editClassroomWindow.ShowDialog();

            bool shouldEditClassroom = editClassroomWindow.DialogResult.Value;
            if (shouldEditClassroom)
            {
                ModelHelper.GetInstance().UpdateUcionica(classroom, classroomCopy);
                UcionicaDAO ucionicaDAO = new UcionicaDAO();
                ucionicaDAO.UpdateUcionica(classroomCopy);

                //var index = korisnici.IndexOf(user);
                //korisnici[index] = userCopy;
            }
            CollectionView.Refresh();
        }

        private void DeleteClassroomButton_Click(object sender, RoutedEventArgs e)
        {
            var result = MessageBox.Show("Jeste li sigurni da zelite da obrisete?",
                "Brisanje", MessageBoxButton.YesNo);
            if (result == MessageBoxResult.Yes)
            {
                var ucionica = ClassroomsGrid.SelectedItem as Ucionica;
                ModelHelper.GetInstance().DeleteUcionica(ucionica);
                UcionicaDAO.DeleteUcionica(ucionica);
                CollectionView.Refresh();
            }
            else if (result == MessageBoxResult.No)
            {
                return;
            }
        }
    }
}
