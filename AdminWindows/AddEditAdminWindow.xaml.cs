﻿using ProjekatPOP.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjekatPOP
{
    /// <summary>
    /// Interaction logic for AddEditInstitutionWindow.xaml
    /// </summary>
    public partial class AddEditAdminWindow : Window
    {

        public Admin Admin { get; set; }

        public enum AdminWindowState { Addition, Edition };

        private AdminWindowState state;

        public AddEditAdminWindow(Admin admin, AdminWindowState adminWindowState = AdminWindowState.Addition)
        {
            InitializeComponent();

            Admin = admin;
            state = adminWindowState;

            RolesComboBox.Items.Add("Admin");

            DataContext = Admin;
        }

        private void SaveAdminButton_Click(object sender, RoutedEventArgs e)
        {
            if (!(state == AdminWindowState.Edition && String.IsNullOrWhiteSpace(AdminPassword.Password)))
            {
                Admin.Lozinka = AdminPassword.Password;
            }

            Admin.Lozinka = AdminPassword.Password;

            string selectedRole = (string)RolesComboBox.SelectedItem;
            if (selectedRole != null)
            {
                enumTipKorisnika tipKorisnika;
                Enum.TryParse(selectedRole, out tipKorisnika);

                Admin.TipKorisnika = tipKorisnika;


                DialogResult = true;
                Close();
            }
        }

        private void DeleteAdminButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }
    }
}
