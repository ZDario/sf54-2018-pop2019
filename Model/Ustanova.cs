﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjekatPOP.Model
{
    public class Ustanova
    {
        private int id;
        private string naziv;
        private string lokacija;
        private bool active;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Naziv
        {
            get { return naziv; }
            set { naziv = value; }
        }

        public string Lokacija
        {
            get { return lokacija; }
            set { lokacija = value; }
        }

        public bool Active { get; set; } = true;

        public Ustanova()
        {
            Naziv = "";
            Lokacija = "";
            Active = true;
        }

        public Ustanova(Ustanova original)
        {
            Id = original.Id;
            Naziv = original.Naziv;
            Lokacija = original.Lokacija;
            Active = original.Active;
        }

        public Ustanova(int id, string naziv, string lokacija, bool active)
        {
            Id = id;
            Naziv = naziv;
            Lokacija = lokacija;
            Active = active;
        }
    }
}
