﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjekatPOP.Model
{
    public class Profesor : Korisnik
    {
        private int idAsistenta;

        public int IdAsistenta { get; set; }

        public Profesor() : base()
        {
            idAsistenta = 0;
        }

        public Profesor(Profesor original) : base()
        {
            IdAsistenta = original.IdAsistenta;
        }


        public Profesor(int idAsistenta) : base()
        {
            IdAsistenta = idAsistenta;
        }
    }
}
