﻿using ProjekatPOP.DAO;
using ProjekatPOP.NewException;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace ProjekatPOP.Model
{
    public class ModelHelper
    {
        public static string ConnectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=BazaPodataka;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        private static ModelHelper instance;

        //public List<Korisnik> Korisnici = new List<Korisnik>();
        public List<Asistent> Asistenti = new List<Asistent>();
        public List<Profesor> Profesori = new List<Profesor>();
        public List<Admin> Admini = new List<Admin>();
        public List<Ustanova> Ustanove = new List<Ustanova>();
        public List<Ucionica> Ucionice = new List<Ucionica>();
        public List<Termin> Termini = new List<Termin>();

        public static ModelHelper GetInstance()
        {
            
            if (instance == null)
            {
                instance = new ModelHelper();
            }

            return instance;
        }




        //KOLEKCIJE PODATAKA
        //public void InitKorisnici()
        //{
        //    Korisnik korisnik1 = new Korisnik();
        //    korisnik1.Ime = "Stanisa";
        //    korisnik1.Prezime = "Stanisic";
        //    korisnik1.Id = "1";
        //    korisnik1.Email = "email@live1.com";
        //    korisnik1.KorisnickoIme = "korisnik1";
        //    korisnik1.Lozinka = "password1";

        //    Korisnici.Add(korisnik1);

        //    Korisnik korisnik2 = new Korisnik();
        //    korisnik2.Ime = "Ljubisa";
        //    korisnik2.Prezime = "Ljubisic";
        //    korisnik2.Id = "2";
        //    korisnik2.Email = "email@live2.com";
        //    korisnik2.KorisnickoIme = "korisnik2";
        //    korisnik2.Lozinka = "password2";

        //    Korisnici.Add(korisnik2);

        //    Korisnik korisnik3 = new Korisnik();
        //    korisnik3.Ime = "Sinisa";
        //    korisnik3.Prezime = "Sinisic";
        //    korisnik3.Id = "3";
        //    korisnik3.Email = "email@live3.com";
        //    korisnik3.KorisnickoIme = "korisnik3";
        //    korisnik3.Lozinka = "password3";

        //    Korisnici.Add(korisnik3);
        //}
        //public void InitAsistenti()
        //{
        //      Asistent asistent1 = new Asistent();
        //      asistent1.Ime = "Stanisa";
        //      asistent1.Prezime = "Stanisic";
        //      asistent1.Id = 1;
        //      asistent1.Email = "email@live1.com";
        //      asistent1.KorisnickoIme = "korisnik1";
        //      asistent1.Lozinka = "password1";

        //      Asistenti.Add(asistent1);
        //}
        //public void InitProfesori()
        //{
        //    Profesor profesor2 = new Profesor();
        //    profesor2.Ime = "Ljubisa";
        //    profesor2.Prezime = "Ljubisic";
        //    profesor2.Id = 2;
        //    profesor2.Email = "email@live2.com";
        //    profesor2.KorisnickoIme = "korisnik2";
        //    profesor2.Lozinka = "password2";

        //    Profesori.Add(profesor2);
        //}
        //public void InitAdmini()
        //{
        //    Admin admin3 = new Admin();
        //    admin3.Ime = "Sinisa";
        //    admin3.Prezime = "Sinisic";
        //    admin3.Id = 3;
        //    admin3.Email = "email@live3.com";
        //    admin3.KorisnickoIme = "korisnik3";
        //    admin3.Lozinka = "password3";
        //    admin3.TipKorisnika = enumTipKorisnika.Admin;

        //    Admini.Add(admin3);
        //}

        //public void InitUstanove()
        //{
        //    Ustanova ustanova1 = new Ustanova();
        //    ustanova1.Id = 123;
        //    ustanova1.Naziv = "FTN";
        //    ustanova1.Lokacija = "Trg Dositeja Obradovica 6";

        //    Ustanove.Add(ustanova1);

        //    Ustanova ustanova2 = new Ustanova();
        //    ustanova2.Id = 124;
        //    ustanova2.Naziv = "PMF";
        //    ustanova2.Lokacija = "Trg Dositeja Obradovica 3";

        //    Ustanove.Add(ustanova2);

        //    Ustanova ustanova3 = new Ustanova();
        //    ustanova3.Id = 125;
        //    ustanova3.Naziv = "Ekonomski Fakultet";
        //    ustanova3.Lokacija = "Dr Sime Milosevica 16";

        //    Ustanove.Add(ustanova3);
        //}

        //public void InitUcionice()
        //{
        //    Ucionica ucionica1 = new Ucionica();
        //    ucionica1.Id = 12343;
        //    ucionica1.BrojUcionice = "A1";
        //    ucionica1.BrojMesta = 15;
        //    ucionica1.TipUcionice = enumTipUcionice.Normalna_Ucionica;

        //    Ucionice.Add(ucionica1);

        //    Ucionica ucionica2 = new Ucionica();
        //    ucionica2.Id = 12344;
        //    ucionica2.BrojUcionice = "A2";
        //    ucionica2.BrojMesta = 20;
        //    ucionica2.TipUcionice = enumTipUcionice.Sa_Racunarima;

        //    Ucionice.Add(ucionica2);

        //    Ucionica ucionica3 = new Ucionica();
        //    ucionica3.Id = 12345;
        //    ucionica3.BrojUcionice = "A3";
        //    ucionica3.BrojMesta = 60;
        //    ucionica3.TipUcionice = enumTipUcionice.Amfiteatar;

        //    Ucionice.Add(ucionica3);
        //}

        //public void InitTermini()
        //{
        //    Termin termin1 = new Termin();
        //    termin1.Id = 678;
        //    termin1.VremeZauzeca = "16:15";
        //    termin1.DaniUNedelji = enumDaniUNedelji.Ponedeljak;
        //    termin1.TipNastave = enumTipNastave.Predavanje;

        //    Termini.Add(termin1);
        //}


        //UPISIVANJE
        //public void WriteKorisniksToFile()
        //{
        //    IFormatter formatter = new BinaryFormatter();
        //    Stream outputStream = new FileStream(@"../../Resources/korisnici.bin", FileMode.Create, FileAccess.Write);

        //    formatter.Serialize(outputStream, Asistenti);
        //    outputStream.Close();
        //}

        //public void WriteAsistentsToFile()
        //{
        //    IFormatter formatter = new BinaryFormatter();
        //    Stream outputStream = new FileStream(@"../../Resources/asistenti.bin", FileMode.Create, FileAccess.Write);

        //    formatter.Serialize(outputStream, Asistenti);
        //    outputStream.Close();
        //}
        //public void WriteProfesorsToFile()
        //{
        //    IFormatter formatter = new BinaryFormatter();
        //    Stream outputStream = new FileStream(@"../../Resources/profesori.bin", FileMode.Create, FileAccess.Write);

        //    formatter.Serialize(outputStream, Profesori);
        //    outputStream.Close();
        //}
        //public void WriteAdminsToFile()
        //{
        //    IFormatter formatter = new BinaryFormatter();
        //    Stream outputStream = new FileStream(@"../../Resources/admini.bin", FileMode.Create, FileAccess.Write);

        //    formatter.Serialize(outputStream, Admini);
        //    outputStream.Close();
        //}

        //public void WriteInstitutionsToFile()
        //{
        //    IFormatter formatter = new BinaryFormatter();
        //    Stream outputStream = new FileStream(@"../../Resources/ustanove.bin", FileMode.Create, FileAccess.Write);

        //    formatter.Serialize(outputStream, Ustanove);
        //    outputStream.Close();
        //}

        //public void WriteClassRooomsToFile()
        //{
        //    IFormatter formatter = new BinaryFormatter();
        //    Stream outputStream = new FileStream(@"../../Resources/ucionice.bin", FileMode.Create, FileAccess.Write);

        //    formatter.Serialize(outputStream, Ucionice);
        //    outputStream.Close();
        //}

        //public void WriteTermsToFile()
        //{
        //    IFormatter formatter = new BinaryFormatter();
        //    Stream outputStream = new FileStream(@"../../Resources/termini.bin", FileMode.Create, FileAccess.Write);

        //    formatter.Serialize(outputStream, Termini);
        //    outputStream.Close();
        //}


        //CITANJE
        //public void ReadKorisniksFromFile()
        //{
        //    IFormatter formatter = new BinaryFormatter();
        //    Stream inputStream = new FileStream(@"../../Resources/korisnici.bin", FileMode.Open, FileAccess.Read);

        //    Korisnici = (List<Korisnik>)formatter.Deserialize(inputStream);
        //    inputStream.Close();
        //}

        //public void ReadAsistentsFromFile()
        //{
        //    IFormatter formatter = new BinaryFormatter();
        //    Stream inputStream = new FileStream(@"../../Resources/asistenti.bin", FileMode.Open, FileAccess.Read);

        //    Asistenti = (List<Asistent>)formatter.Deserialize(inputStream);
        //    inputStream.Close();
        //}
        //public void ReadProfesorsFromFile()
        //{
        //    IFormatter formatter = new BinaryFormatter();
        //    Stream inputStream = new FileStream(@"../../Resources/profesori.bin", FileMode.Open, FileAccess.Read);

        //    Profesori = (List<Profesor>)formatter.Deserialize(inputStream);
        //    inputStream.Close();
        //}
        //public void ReadAdministratorsFromFile()
        //{
        //    IFormatter formatter = new BinaryFormatter();
        //    Stream inputStream = new FileStream(@"../../Resources/admini.bin", FileMode.Open, FileAccess.Read);

        //    Admini = (List<Admin>)formatter.Deserialize(inputStream);
        //    inputStream.Close();
        //}

        //public void ReadInstitutionsFromFile()
        //{
        //    IFormatter formatter = new BinaryFormatter();
        //    Stream inputStream = new FileStream(@"../../Resources/ustanove.bin", FileMode.Open, FileAccess.Read);

        //    Ustanove = (List<Ustanova>)formatter.Deserialize(inputStream);
        //    inputStream.Close();
        //}

        //public void ReadClassroomsFromFile()
        //{
        //    IFormatter formatter = new BinaryFormatter();
        //    Stream inputStream = new FileStream(@"../../Resources/ucionice.bin", FileMode.Open, FileAccess.Read);

        //    Ucionice = (List<Ucionica>)formatter.Deserialize(inputStream);
        //    inputStream.Close();
        //}

        //public void ReadTermsFromFile()
        //{
        //    IFormatter formatter = new BinaryFormatter();
        //    Stream inputStream = new FileStream(@"../../Resources/termini.bin", FileMode.Open, FileAccess.Read);

        //    Termini = (List<Termin>)formatter.Deserialize(inputStream);
        //    inputStream.Close();
        //}





        //DOBIJANJE NA OSNOVU INDEXA
        //public Korisnik GetKorisnik(string id)
        //{
        //    foreach (var korisnik in Korisniki)
        //    {
        //        if (korisnik.Id == id)
        //        {
        //            return korisnik;
        //        }
        //    }
        //    return null;
        //}

        public Asistent GetAsistent(int id)
        {
            foreach(var asistent in Asistenti)
            {
                if(asistent.Id == id)
                {
                    return asistent;
                }
            }
            return null;
        }
        public Profesor GetProfesor(int id)
        {
            foreach (var profesor in Profesori)
            {
                if (profesor.Id == id)
                {
                    return profesor;
                }
            }
            return null;
        }
        public Admin GetAdmin(int id)
        {
            foreach (var admin in Admini)
            {
                if (admin.Id == id)
                {
                    return admin;
                }
            }
            return null;
        }

        public Ustanova GetUstanova(int id)
        {
            foreach (var ustanova in Ustanove)
            {
                if (ustanova.Id == id)
                {
                    return ustanova;
                }
            }
            return null;
        }
        public Ucionica GetUcionica(int id)
        {
            foreach (var ucionica in Ucionice)
            {
                if (ucionica.Id == id)
                {
                    return ucionica;
                }
            }
            return null;
        }

        public Termin GetTermin(int id)
        {
            foreach (var termin in Termini)
            {
                if (termin.Id == id)
                {
                    return termin;
                }
            }
            return null;
        }



        //BRISANJE
        //public void DeleteKorisnik(Korisnik k)
        //{
        //    //Asistenti.Remove(k);
        //    int i = 0;
        //    for (; Korisnici.Count > i; i++)
        //    {
        //        if (Korisnici.ElementAt(i).Id == k.Id)
        //        {
        //            Korisnici.ElementAt(i).Active = false;
        //            break;
        //        }
        //    }
        //}
        public void DeleteAsistent(Asistent a)
        {
            //Asistenti.Remove(k);
            int i = 0;
            for(; Asistenti.Count > i;  i++)
            {
                if(Asistenti.ElementAt(i).Id == a.Id)
                {
                    Asistenti.ElementAt(i).Active = false;
                    break;
                }
            }
        }
        public void DeleteProfesor(Profesor p)
        {
            //Profesori.Remove(k);
            int i = 0;
            for (; Profesori.Count > i; i++)
            {
                if (Profesori.ElementAt(i).Id == p.Id)
                {
                    Profesori.ElementAt(i).Active = false;
                    break;
                }
            }
        }
        public void DeleteAdmin(Admin ad)
        {
            //Korisnici.Remove(k);
            int i = 0;
            for (; Admini.Count > i; i++)
            {
                if (Admini.ElementAt(i).Id == ad.Id)
                {
                    Admini.ElementAt(i).Active = false;
                    break;
                }
            }
        }

        public void DeleteUstanova(Ustanova Us)
        {
            //Korisnici.Remove(k);
            int i = 0;
            for (; Ustanove.Count > i; i++)
            {
                if (Ustanove.ElementAt(i).Id == Us.Id)
                {
                    Ustanove.ElementAt(i).Active = false;
                    break;
                }
            }
        }
        public void DeleteUcionica(Ucionica Uc)
        {
            //Korisnici.Remove(k);
            int i = 0;
            for (; Ucionice.Count > i; i++)
            {
                if (Ucionice.ElementAt(i).Id == Uc.Id)
                {
                    Ucionice.ElementAt(i).Active = false;
                    break;
                }
            }
        }

        public void DeleteTermin(Termin t)
        {
            //Korisnici.Remove(k);
            int i = 0;
            for (; Termini.Count > i; i++)
            {
                if (Termini.ElementAt(i).Id == t.Id)
                {
                    Termini.ElementAt(i).Active = false;
                    break;
                }
            }
        }



        //DODAVANJE
        //public void AddKorisnik(Korisnik k)
        //{
        //    int i = 0;
        //    for (; Korisnici.Count > i; i++)
        //    {
        //        if (Korisnici.ElementAt(i).KorisnickoIme == k.KorisnickoIme)
        //        {
        //            throw new UserExistsException("Korisnik sa ovakvim korisnickim imenom postoji!");
        //        }
        //    }
        //    Korisnici.Add(k);
        //}
        public void AddAsistent(Asistent a)
        {
            int i = 0;
            for(; Asistenti.Count > i; i++)
            {
                if(Asistenti.ElementAt(i).KorisnickoIme == a.KorisnickoIme)
                {
                    throw new UserExistsException("Asistent sa ovakvim korisnickim imenom postoji!");
                }
            }
            Asistenti.Add(a);
        }
        public void AddProfesor(Profesor p)
        {
            int i = 0;
            for (; Profesori.Count > i; i++)
            {
                if (Profesori.ElementAt(i).KorisnickoIme == p.KorisnickoIme)
                {
                    throw new UserExistsException("Profesor sa ovakvim korisnickim imenom postoji!");
                }
            }
            Profesori.Add(p);
        }
        public void AddAdmin(Admin ad)
        {
            int i = 0;
            for (; Admini.Count > i; i++)
            {
                if (Admini.ElementAt(i).KorisnickoIme == ad.KorisnickoIme)
                {
                    throw new UserExistsException("Administrator sa ovakvim korisnickim imenom postoji!");
                }
            }
            Admini.Add(ad);
        }

        public void AddUstanova(Ustanova Us)
        {
            int i = 0;
            for (; Ustanove.Count > i; i++)
            {
                if (Ustanove.ElementAt(i).Naziv == Us.Naziv)
                {
                    throw new InstitutionExistsException("Ustanova sa ovakvom sifrom postoji!");
                }
            }
            Ustanove.Add(Us);
        }

        public void AddUcionica(Ucionica Uc)
        {
            int i = 0;
            for (; Ucionice.Count > i; i++)
            {
                if (Ucionice.ElementAt(i).Id == Uc.Id)
                {
                    throw new ClassroomExistsException("Ucionica sa ovakvom sifrom postoji!");
                }
            }
            Ucionice.Add(Uc);
        }

        public void AddTermin(Termin t)
        {
            int i = 0;
            for (; Termini.Count > i; i++)
            {
                if (Termini.ElementAt(i).Id == t.Id)
                {
                    throw new UserExistsException("Termin sa ovakvom sifrom postoji!");
                }
            }
            Termini.Add(t);
        }

        //public void UpdateKorisnik(Korisnik k)
        //{
        //    int i = 0;
        //    for(;Korisnici.Count > i; i++)
        //    {
        //        if(Korisnici.ElementAt(i).KorisnickoIme == k.KorisnickoIme)
        //        {
        //            Korisnici[i] = k;
        //        }
        //        else
        //        {
        //            throw new Exception("Korisnik sa ovakvim korisnickim imenom ne postoji!");
        //        }
        //    }   
        //}




        //UPDEJTOVANJE
        //public void UpdateKorisnik(Korisnik original, Korisnik copy)
        //{
        //    int index = Korisnici.IndexOf(original);
        //    Korisnici[index] = copy;
        //}
        public void UpdateAsistent(Asistent original, Asistent copy)
        {
            int index = Asistenti.IndexOf(original);
            Asistenti[index] = copy;
            AsistentDAO asistentDAO = new AsistentDAO();
            asistentDAO.UpdateAsistent(copy);
        }
        public void UpdateProfesor(Profesor original, Profesor copy)
        {
            int index = Profesori.IndexOf(original);
            Profesori[index] = copy;
            ProfesorDAO profesorDAO = new ProfesorDAO();
            profesorDAO.UpdateProfesor(copy);
        }
        public void UpdateAdmin(Admin original, Admin copy)
        {
            int index = Admini.IndexOf(original);
            Admini[index] = copy;
            AdminDAO adminDAO = new AdminDAO();
            adminDAO.UpdateAdmin(copy);
        }

        public void UpdateUstanova(Ustanova original, Ustanova copy)
        {
            int index = Ustanove.IndexOf(original);
            Ustanove[index] = copy;
            UstanovaDAO ustanovaDAO = new UstanovaDAO();
            ustanovaDAO.UpdateUstanova(copy);
        }

        public void UpdateUcionica(Ucionica original, Ucionica copy)
        {
            int index = Ucionice.IndexOf(original);
            Ucionice[index] = copy;
            UcionicaDAO ucionicaDAO = new UcionicaDAO();
            ucionicaDAO.UpdateUcionica(copy);
        }

        public void UpdateTermin(Termin original, Termin copy)
        {
            int index = Termini.IndexOf(original);
            Termini[index] = copy;
            TerminDAO terminDAO = new TerminDAO();
            terminDAO.UpdateTermin(copy);
        }



        //DOBIJANJE PO KOR.IMENU I INDEXU
        //public List<Korisnik> GetKorisnikByKorIme(string korisnickoIme)
        //{
        //    List<Korisnik> VraceniKorisnici = new List<Korisnik>();
        //    foreach( var korisnik in Korisnici)
        //    {
        //        if (korisnik.KorisnickoIme.Contains(korisnickoIme))
        //        {
        //            VraceniKorisnici.Add(korisnik);
        //        }
        //    }
        //    return VraceniKorisnici;
        //}
        public List<Asistent> GetAsistentByKorIme(string korisnickoIme)
        {
            List<Asistent> VraceniAsistenti = new List<Asistent>();
            foreach (var asistent in Asistenti)
            {
                if (asistent.KorisnickoIme.Contains(korisnickoIme))
                {
                    VraceniAsistenti.Add(asistent);
                }
            }
            return VraceniAsistenti;
        }
        public List<Profesor> GetProfesorByKorIme(string korisnickoIme)
        {
            List<Profesor> VraceniProfesori = new List<Profesor>();
            foreach (var profesor in Profesori)
            {
                if (profesor.KorisnickoIme.Contains(korisnickoIme))
                {
                    VraceniProfesori.Add(profesor);
                }
            }
            return VraceniProfesori;
        }
        public List<Admin> GetAdminByKorIme(string korisnickoIme)
        {
            List<Admin> VraceniAdmini = new List<Admin>();
            foreach (var admin in Admini)
            {
                if (admin.KorisnickoIme.Contains(korisnickoIme))
                {
                    VraceniAdmini.Add(admin);
                }
            }
            return VraceniAdmini;
        }

        public List<Ustanova> GetUstanovaById(int id)
        {
            List<Ustanova> VraceneUstanove = new List<Ustanova>();
            foreach (var ustanova in Ustanove)
            {
                if (ustanova.Id.Equals(id))
                {
                    VraceneUstanove.Add(ustanova);
                }
            }
            return VraceneUstanove;
        }

        public List<Ucionica> GetUcionicaById(int id)
        {
            List<Ucionica> VraceneUcionice = new List<Ucionica>();
            foreach (var ucionica in Ucionice)
            {
                if (ucionica.Id.Equals(id))
                {
                    VraceneUcionice.Add(ucionica);
                }
            }
            return VraceneUcionice;
        }

        public List<Termin> GetTerminById(int id)
        {
            List<Termin> VraceniTermini = new List<Termin>();
            foreach (var termin in Termini)
            {
                if (termin.Id.Equals(id))
                {
                    VraceniTermini.Add(termin);
                }
            }
            return VraceniTermini;
        }

        //Ako imamo original konstruktor i copy konstruktor
        //public void Update(Korisnik original, Korisnik copy)
        //{
        //    int index = Korisnici.IndexOf(original);
        //    Korisnici[index] = copy;
        //}



        //SORTIRANJE
        //public void SortUsersById()
        //{
        //    Korisnici.OrderBy(u => u.Id);

        //}
        //public void SortUsersByEmail()
        //{
        //    Korisnici.OrderBy(u => u.Email);
        //}
    }
}
