﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjekatPOP.Model
{
    public class Asistent : Korisnik
    {
        private int idProfesora;


        public int IdProfesora { get; set; }


        public Asistent() : base()
        {
            idProfesora = 0;
        }

        public Asistent(Asistent original) : base()
        {
            IdProfesora = original.IdProfesora;
        }


        public Asistent(int idProfesora) : base()
        {
            IdProfesora = idProfesora;
        }
    }
}
