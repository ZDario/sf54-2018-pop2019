﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjekatPOP.Model
{
    public class Termin
    {
        private int id;
        private string vremeZauzeca;
        private enumDaniUNedelji daniUNedelji;
        private enumTipNastave tipNastave;
        private int idAsistenta;
        private int idProfesora;
        private bool active;


        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string VremeZauzeca
        {
            get { return vremeZauzeca; }
            set { vremeZauzeca = value; }
        }

        public enumDaniUNedelji DaniUNedelji
        {
            get { return daniUNedelji; }
            set { daniUNedelji = value; }
        }
        public enumTipNastave TipNastave
        {
            get { return tipNastave; }
            set { tipNastave = value; }
        }

        public int IdAsistenta
        {
            get { return idAsistenta; }
            set { idAsistenta = value; }
        }

        public int IdProfesora
        {
            get { return idProfesora; }
            set { idProfesora = value; }
        }

        public bool Active { get; set; } = true;


        public Termin()
        {
            VremeZauzeca = "";
            DaniUNedelji = enumDaniUNedelji.Ponedeljak;
            TipNastave = enumTipNastave.Predavanje;
            IdAsistenta = 0;
            IdProfesora = 0;
            Active = true;
        }

        public Termin(Termin original)
        {
            Id = original.Id;
            VremeZauzeca = original.VremeZauzeca;
            DaniUNedelji = original.daniUNedelji;
            TipNastave = original.tipNastave;
            IdAsistenta = original.IdAsistenta;
            IdProfesora = original.IdProfesora;
            Active = original.Active;
        }

        public Termin(int id, string vremeZauzeca, enumDaniUNedelji daniUNedelji, enumTipNastave tipNastave,int idAsistenta,int idProfesora, bool active)
        {
            Id = id;
            VremeZauzeca = vremeZauzeca;
            DaniUNedelji = daniUNedelji;
            TipNastave = tipNastave;
            IdAsistenta = idAsistenta;
            IdProfesora = idProfesora;
            Active = active;
        }
    }
}
