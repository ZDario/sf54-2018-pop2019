﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjekatPOP.Model
{
    [Serializable]
    public class Korisnik
    {
        private int id;
        private string ime;
        private string prezime;
        private string email;
        private string korisnickoIme;
        private string lozinka;
        private enumTipKorisnika tipKorisnika;
        private bool active;
        private int godinaRodjenja;


        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Ime
        {
            get { return ime; }
            set { ime = value; }
        }


        public string Prezime
        {
            get { return prezime; }
            set { prezime = value; }
        }


        public string Email
        {
            get { return email; }
            set { email = value; }
        }


        public string KorisnickoIme
        {
            get { return korisnickoIme; }
            set
            {
                if (value != "")
                {
                    korisnickoIme = value;
                }
            }
        }

        public int GodinaRodjenja { get; set; }

        public string Lozinka
        {
            get { return lozinka; }
            set { lozinka = value; }
        }


        public enumTipKorisnika TipKorisnika
        {
            get { return tipKorisnika; }
            set
            {
                tipKorisnika = value;
            }
        }

        public bool Active { get; set; } = true;


        public Korisnik()
        {
            Ime = "";
            Prezime = "";
            Email = "";
            KorisnickoIme = "";
            GodinaRodjenja = 0;
            Lozinka = "";
            TipKorisnika = enumTipKorisnika.Asistent;
            Active = true;
        }

        public Korisnik(Korisnik original)
        {
            Id = original.Id;
            Ime = original.Ime;
            Prezime = original.Prezime;
            Email = original.Email;
            KorisnickoIme = original.KorisnickoIme;
            GodinaRodjenja = original.GodinaRodjenja;
            Lozinka = original.Lozinka;
            TipKorisnika = original.tipKorisnika;
            Active = original.Active;
        }


        public Korisnik(int id, string ime, string prezime, string email, string korisnickoIme, int godinaRodjenja, string lozinka, enumTipKorisnika tipKorisnika, bool active)
        {
            Id = id;
            Ime = ime;
            Prezime = prezime;
            Email = email;
            KorisnickoIme = korisnickoIme;
            GodinaRodjenja = godinaRodjenja;
            Lozinka = lozinka;
            TipKorisnika = tipKorisnika;
            Active = active;
        }
    }
}
