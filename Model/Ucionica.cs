﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjekatPOP.Model
{
    public class Ucionica
    {
        private int id;
        private string brojUcionice;
        private int brojMesta;
        private enumTipUcionice tipUcionice;
        private bool active;
        private int idUstanove;
        public int IdUstanove { get; set; }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string BrojUcionice
        {
            get { return brojUcionice; }
            set { brojUcionice = value; }
        }

        public int BrojMesta
        {
            get { return brojMesta; }
            set { brojMesta = value; }
        }

        public enumTipUcionice TipUcionice
        {
            get { return tipUcionice; }
            set
            {
                tipUcionice = value;
            }
        }

        public bool Active { get; set; } = true;

        public Ucionica()
        {
            BrojUcionice = "";
            BrojMesta = 0;
            TipUcionice = enumTipUcionice.Normalna_Ucionica;
            idUstanove = 0;
            Active = true;
        }

        public Ucionica(Ucionica original)
        {
            Id = original.Id;
            BrojUcionice = original.BrojUcionice;
            BrojMesta = original.BrojMesta;
            TipUcionice = original.TipUcionice;
            IdUstanove = original.IdUstanove;
            Active = original.Active;
        }

        public Ucionica(int id, string brojUcionice, int brojMesta, enumTipUcionice tipUcionice,int idUstanove , bool active)
        {
            Id = id;
            BrojUcionice = brojUcionice;
            BrojMesta = brojMesta;
            TipUcionice = tipUcionice;
            IdUstanove = idUstanove;
            Active = active;
        }
    }
}
