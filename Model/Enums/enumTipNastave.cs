﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjekatPOP.Model
{
    public enum enumTipNastave
    {
        Predavanje,
        Auditorne_Vezbe,
        Racunarske_Vezbe,
        Konsultacije
    }
}
