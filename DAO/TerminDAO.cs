﻿using ProjekatPOP.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjekatPOP.DAO
{
    class TerminDAO
    {
        public static int CreateTermin(Termin termin)
        {
            using (SqlConnection conn = new SqlConnection(ModelHelper.ConnectionString))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"
                    INSERT INTO Termin (VREMEZAUZECA, DANIUNEDELJI, TIPNASTAVE, ASISTENT, PROFESOR, ACTIVE)
                    output inserted.id VALUES (@VREMEZAUZECA, @DANIUNEDELJI, @TIPNASTAVE, @ASISTENT, @PROFESOR, @ACTIVE)";

                command.Parameters.Add(new SqlParameter("VREMEZAUZECA", termin.VremeZauzeca));
                command.Parameters.Add(new SqlParameter("DANIUNEDELJI", termin.DaniUNedelji));
                command.Parameters.Add(new SqlParameter("TIPNASTAVE", termin.TipNastave));
                command.Parameters.Add(new SqlParameter("ASISTENT", termin.IdAsistenta));
                command.Parameters.Add(new SqlParameter("PROFESOR", termin.IdProfesora));
                command.Parameters.Add(new SqlParameter("ACTIVE", termin.Active));

                int id = (int)command.ExecuteScalar();
                return id;
            }
        }

        public void UpdateTermin(Termin termin)
        {
            using (SqlConnection sqlConnection = new SqlConnection(ModelHelper.ConnectionString))
            {
                sqlConnection.Open();

                SqlCommand command = sqlConnection.CreateCommand();
                command.CommandText = @"
                        UPDATE Termin SET
                        VREMEZAUZECA=@VREMEZAUZECA, DANIUNEDELJI=@DANIUNEDELJI, TIPNASTAVE=@TIPNASTAVE,
                        ASISTENT=@ASISTENT, PROFESOR=@PROFESOR, ACTIVE=@ACTIVE WHERE ID=@ID";

                command.Parameters.Add(new SqlParameter("VREMEZAUZECA", termin.VremeZauzeca));
                command.Parameters.Add(new SqlParameter("DANIUNEDELJI", termin.DaniUNedelji.ToString()));
                command.Parameters.Add(new SqlParameter("TIPNASTAVE", termin.TipNastave.ToString()));
                command.Parameters.Add(new SqlParameter("ASISTENT", termin.IdAsistenta));
                command.Parameters.Add(new SqlParameter("PROFESOR", termin.IdProfesora));
                command.Parameters.Add(new SqlParameter("ACTIVE", termin.Active));
                command.Parameters.Add(new SqlParameter("ID", termin.Id));

                command.ExecuteNonQuery();
            }
        }

        public List<Termin> SelectTermin()
        {
            List<Termin> retVal = new List<Termin>();

            using (SqlConnection connection = new SqlConnection(ModelHelper.ConnectionString))
            {
                connection.Open();

                DataSet ds = new DataSet();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM TERMIN";


                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;
                dataAdapter.Fill(ds, "Termin");


                foreach (DataRow row in ds.Tables["Termin"].Rows)
                {
                    Termin termin = new Termin();
                    termin.Id = (int)row["Id"];
                    string VremeZauzeca = (string)row["VremeZauzeca"];
                    string DaniUNedelji = (string)row["DaniUNedelji"];
                    termin.DaniUNedelji = (enumDaniUNedelji)Enum.Parse(typeof(enumDaniUNedelji), DaniUNedelji);
                    string TipNastave = (string)row["TipNastave"];
                    termin.TipNastave = (enumTipNastave)Enum.Parse(typeof(enumTipNastave), TipNastave);
                    termin.IdAsistenta = (int)row["Asistent"];
                    termin.IdProfesora = (int)row["Profesor"];
                    termin.Active = (bool)row["Active"];


                    retVal.Add(termin);
                }
            }
            return retVal;
        }

        public Termin SelectTerminById(int id)
        {
            using (SqlConnection connection = new SqlConnection(ModelHelper.ConnectionString))
            {
                connection.Open();

                DataSet ds = new DataSet();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM TERMIN WHERE Id = @Id";

                command.Parameters.Add(new SqlParameter("Id", id));


                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;
                dataAdapter.Fill(ds, "Termin");

                foreach (DataRow row in ds.Tables["Termin"].Rows)
                {
                    Termin termin = new Termin();
                    termin.Id = (int)row["Id"];
                    termin.VremeZauzeca = (string)row["VremeZauzeca"];
                    termin.DaniUNedelji = (enumDaniUNedelji)row["DaniUNedelji"];
                    termin.TipNastave = (enumTipNastave)row["TipNastave"];
                    termin.IdAsistenta = (int)row["Asistent"];
                    termin.IdProfesora = (int)row["Profesor"];
                    termin.Active = (bool)row["Active"];

                    return termin;
                }
            }
            return null;
        }

        public static void DeleteTermin(Termin termin)
        {
            using (SqlConnection conn = new SqlConnection(ModelHelper.ConnectionString))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"DELETE FROM TERMIN WHERE ID=@Id";

                command.Parameters.Add(new SqlParameter("@Id", termin.Id));

                command.ExecuteNonQuery();
            }
        }
    }
}
