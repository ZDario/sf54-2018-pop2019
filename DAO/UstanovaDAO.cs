﻿using ProjekatPOP.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjekatPOP.DAO
{
    class UstanovaDAO
    {
        public static int CreateUstanova(Ustanova ustanova)
        {
            using(SqlConnection conn = new SqlConnection(ModelHelper.ConnectionString))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"
                    INSERT INTO Ustanova (NAZIV, LOKACIJA, ACTIVE)
                    output inserted.id VALUES (@NAZIV, @LOKACIJA, @ACTIVE)";

                command.Parameters.Add(new SqlParameter("NAZIV", ustanova.Naziv));
                command.Parameters.Add(new SqlParameter("LOKACIJA", ustanova.Lokacija));
                command.Parameters.Add(new SqlParameter("ACTIVE", ustanova.Active));

                int id = (int)command.ExecuteScalar();
                return id;
            }
        }

        public void UpdateUstanova(Ustanova ustanova)
        {
            using (SqlConnection sqlConnection = new SqlConnection(ModelHelper.ConnectionString))
            {
                sqlConnection.Open();

                SqlCommand command = sqlConnection.CreateCommand();
                command.CommandText = @"
                        UPDATE Ustanova SET
                        NAZIV=@NAZIV, LOKACIJA=@LOKACIJA, ACTIVE=@ACTIVE
                        WHERE ID=@ID";

                command.Parameters.Add(new SqlParameter("NAZIV", ustanova.Naziv));
                command.Parameters.Add(new SqlParameter("LOKACIJA", ustanova.Lokacija));
                command.Parameters.Add(new SqlParameter("ACTIVE", ustanova.Active));
                command.Parameters.Add(new SqlParameter("ID", ustanova.Id));

                command.ExecuteNonQuery();
            }
        }

        public List<Ustanova> SelectUstanova()
        {
            List<Ustanova> retVal = new List<Ustanova>();

            using(SqlConnection connection = new SqlConnection(ModelHelper.ConnectionString))
            {
                connection.Open();

                DataSet ds = new DataSet();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM USTANOVA";


                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;
                dataAdapter.Fill(ds, "Ustanova");

                //var dataTable = ds.Tables["Ustanova"].AsEnumerable();
               // dataTable.Where(row => ((string)row("Naziv").Contains("FTN")).Count();

                foreach(DataRow row in ds.Tables["Ustanova"].Rows)
                {
                    Ustanova ustanova = new Ustanova();
                    ustanova.Id = (int) row["Id"];
                    ustanova.Naziv = (string) row["Naziv"];
                    ustanova.Lokacija = (string) row["Lokacija"];
                    ustanova.Active = (bool) row["Active"];

                    retVal.Add(ustanova);
                }
            }

            return retVal;
        }

        public Ustanova SelectUstanovaById(int id)
        {
          using (SqlConnection connection = new SqlConnection(ModelHelper.ConnectionString))
            {
                connection.Open();

                DataSet ds = new DataSet();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM USTANOVA WHERE Id = @Id";

                command.Parameters.Add(new SqlParameter("Id", id));


                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;
                dataAdapter.Fill(ds, "Ustanova");

                //var dataTable = ds.Tables["Ustanova"].AsEnumerable();
                // dataTable.Where(row => ((string)row("Naziv").Contains("FTN")).Count();

                foreach (DataRow row in ds.Tables["Ustanova"].Rows)
                {
                    Ustanova ustanova = new Ustanova();
                    ustanova.Id = (int)row["Id"];
                    ustanova.Naziv = (string)row["Naziv"];
                    ustanova.Lokacija = (string)row["Lokacija"];
                    ustanova.Active = (bool)row["Active"];

                    return ustanova;
                }
            }
            return null;
        }

        public static void DeleteUstanova(Ustanova ustanova)
        {
            using (SqlConnection conn = new SqlConnection(ModelHelper.ConnectionString))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"DELETE FROM USTANOVA WHERE ID=@Id";

                command.Parameters.Add(new SqlParameter("@Id", ustanova.Id));

                command.ExecuteNonQuery();
            }
        }
    }
}
