﻿using ProjekatPOP.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjekatPOP.DAO
{
    class ProfesorDAO
    {
        public static int CreateProfesor(Profesor profesor)
        {
            using (SqlConnection conn = new SqlConnection(ModelHelper.ConnectionString))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"
                    INSERT INTO Profesor (IME, PREZIME, EMAIL, KORISNICKOIME, LOZINKA, TIPKORISNIKA, GODINARODJENJA, ASISTENT, ACTIVE)
                    output inserted.id VALUES (@IME, @PREZIME, @EMAIL, @KORISNICKOIME, @LOZINKA, @TIPKORISNIKA, @GODINARODJENJA, @ASISTENT, @ACTIVE)";

                command.Parameters.Add(new SqlParameter("IME", profesor.Ime));
                command.Parameters.Add(new SqlParameter("PREZIME", profesor.Prezime));
                command.Parameters.Add(new SqlParameter("EMAIL", profesor.Email));
                command.Parameters.Add(new SqlParameter("KORISNICKOIME", profesor.KorisnickoIme));
                command.Parameters.Add(new SqlParameter("LOZINKA", profesor.Lozinka));
                command.Parameters.Add(new SqlParameter("TIPKORISNIKA", profesor.TipKorisnika));
                command.Parameters.Add(new SqlParameter("GODINARODJENJA", profesor.GodinaRodjenja));
                command.Parameters.Add(new SqlParameter("ASISTENT", profesor.IdAsistenta));
                command.Parameters.Add(new SqlParameter("ACTIVE", profesor.Active));

                int id = (int)command.ExecuteScalar();
                return id;
            }
        }

        public void UpdateProfesor(Profesor profesor)
        {
            using (SqlConnection sqlConnection = new SqlConnection(ModelHelper.ConnectionString))
            {
                sqlConnection.Open();

                SqlCommand command = sqlConnection.CreateCommand();
                command.CommandText = @"
                        UPDATE Profesor SET
                        IME=@IME, PREZIME=@PREZIME, EMAIL=@EMAIL, KORISNICKOIME=@KORISNICKOIME,
                        LOZINKA=@LOZINKA, TIPKORISNIKA=@TIPKORISNIKA, GODINARODJENJA=@GODINARODJENJA, 
                        ASISTENt=@ASISTENT, ACTIVE=@ACTIVE WHERE ID=@ID";

                command.Parameters.Add(new SqlParameter("IME", profesor.Ime));
                command.Parameters.Add(new SqlParameter("PREZIME", profesor.Prezime));
                command.Parameters.Add(new SqlParameter("EMAIL", profesor.Email));
                command.Parameters.Add(new SqlParameter("KORISNICKOIME", profesor.KorisnickoIme));
                command.Parameters.Add(new SqlParameter("LOZINKA", profesor.Lozinka));
                command.Parameters.Add(new SqlParameter("TIPKORISNIKA", profesor.TipKorisnika.ToString()));
                command.Parameters.Add(new SqlParameter("GODINARODJENJA", profesor.GodinaRodjenja));
                command.Parameters.Add(new SqlParameter("ASISTENT", profesor.IdAsistenta));
                command.Parameters.Add(new SqlParameter("ACTIVE", profesor.Active));
                command.Parameters.Add(new SqlParameter("ID", profesor.Id));

                command.ExecuteNonQuery();
            }
        }

        public List<Profesor> SelectProfesor()
        {
            List<Profesor> retVal = new List<Profesor>();

            using (SqlConnection connection = new SqlConnection(ModelHelper.ConnectionString))
            {
                connection.Open();

                DataSet ds = new DataSet();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM PROFESOR";


                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;
                dataAdapter.Fill(ds, "Profesor");


                foreach (DataRow row in ds.Tables["Profesor"].Rows)
                {
                    Profesor profesor = new Profesor();
                    profesor.Id = (int)row["Id"];
                    profesor.Ime = (string)row["Ime"];
                    profesor.Prezime = (string)row["Prezime"];
                    profesor.Email = (string)row["Email"];
                    profesor.KorisnickoIme = (string)row["KorisnickoIme"];
                    profesor.Lozinka = (string)row["Lozinka"];
                    string TipKorisnika = (string)row["TipKorisnika"];
                    profesor.TipKorisnika = (enumTipKorisnika)Enum.Parse(typeof(enumTipKorisnika), TipKorisnika);
                    profesor.GodinaRodjenja = (int)row["GodinaRodjenja"];
                    profesor.IdAsistenta = (int)row["Asistent"];
                    profesor.Active = (bool)row["Active"];


                    retVal.Add(profesor);
                }
            }
            return retVal;
        }

        public Profesor SelectProfesorById(int id)
        {
            using (SqlConnection connection = new SqlConnection(ModelHelper.ConnectionString))
            {
                connection.Open();

                DataSet ds = new DataSet();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM PROFESOR WHERE Id = @Id";

                command.Parameters.Add(new SqlParameter("Id", id));


                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;
                dataAdapter.Fill(ds, "Profesor");

                foreach (DataRow row in ds.Tables["Profesor"].Rows)
                {
                    Profesor profesor = new Profesor();
                    profesor.Id = (int)row["Id"];
                    profesor.Ime = (string)row["Ime"];
                    profesor.Prezime = (string)row["Prezime"];
                    profesor.Email = (string)row["Email"];
                    profesor.KorisnickoIme = (string)row["KorisnickoIme"];
                    profesor.Lozinka = (string)row["Lozinka"];
                    profesor.TipKorisnika = (enumTipKorisnika)row["TipKorisnika"];
                    profesor.GodinaRodjenja = (int)row["GodinaRodjenja"];
                    profesor.IdAsistenta = (int)row["Asistent"];
                    profesor.Active = (bool)row["Active"];

                    return profesor;
                }
            }
            return null;
        }

        public static void DeleteProfesor(Profesor profesor)
        {
            using (SqlConnection conn = new SqlConnection(ModelHelper.ConnectionString))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"DELETE FROM PROFESOR WHERE ID=@Id";

                command.Parameters.Add(new SqlParameter("@Id", profesor.Id));

                command.ExecuteNonQuery();
            }
        }
    }
}
