﻿using ProjekatPOP.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjekatPOP.DAO
{
    class UcionicaDAO
    {
        public static int CreateUcionica(Ucionica ucionica)
        {
            using (SqlConnection conn = new SqlConnection(ModelHelper.ConnectionString))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"
                    INSERT INTO Ucionica (BROJUCIONICE, BROJMESTA, TIPUCIONICE, IDUSTANOVE, ACTIVE)
                    output inserted.id VALUES (@BROJUCIONICE, @BROJMESTA, @TIPUCIONICE, @IDUSTANOVE, @ACTIVE)";

                command.Parameters.Add(new SqlParameter("BROJUCIONICE", ucionica.BrojUcionice));
                command.Parameters.Add(new SqlParameter("BROJMESTA", ucionica.BrojMesta));
                command.Parameters.Add(new SqlParameter("TIPUCIONICE", ucionica.TipUcionice));
                command.Parameters.Add(new SqlParameter("IDUSTANOVE", ucionica.IdUstanove));
                command.Parameters.Add(new SqlParameter("ACTIVE", ucionica.Active));

                int id = (int)command.ExecuteScalar();
                return id;
            }
        }

        public void UpdateUcionica(Ucionica ucionica)
        {
            using (SqlConnection sqlConnection = new SqlConnection(ModelHelper.ConnectionString))
            {
                sqlConnection.Open();

                SqlCommand command = sqlConnection.CreateCommand();
                command.CommandText = @"
                        UPDATE Ucionica SET
                        BROJUCIONICE=@BROJUCIONICE, BROJMESTA=@BROJMESTA, TIPUCIONICE=@TIPUCIONICE,
                        IDUSTANOVE=@IDUSTANOVE, ACTIVE=@ACTIVE WHERE ID=@ID";

                command.Parameters.Add(new SqlParameter("BROJUCIONICE", ucionica.BrojUcionice));
                command.Parameters.Add(new SqlParameter("BROJMESTA", ucionica.BrojMesta));
                command.Parameters.Add(new SqlParameter("TIPUCIONICE", ucionica.TipUcionice.ToString()));
                command.Parameters.Add(new SqlParameter("IDUSTANOVE", ucionica.IdUstanove));
                command.Parameters.Add(new SqlParameter("ACTIVE", ucionica.Active));
                command.Parameters.Add(new SqlParameter("ID", ucionica.Id));

                command.ExecuteNonQuery();
            }
        }

        public List<Ucionica> SelectUcionica()
        {
            List<Ucionica> retVal = new List<Ucionica>();

            using (SqlConnection connection = new SqlConnection(ModelHelper.ConnectionString))
            {
                connection.Open();

                DataSet ds = new DataSet();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM UCIONICA";


                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;
                dataAdapter.Fill(ds, "Ucionica");


                foreach (DataRow row in ds.Tables["Ucionica"].Rows)
                {
                    Ucionica ucionica = new Ucionica();
                    ucionica.Id = (int)row["Id"];
                    ucionica.BrojUcionice = (string)row["BrojUcionice"];
                    ucionica.BrojMesta = (int)row["BrojMesta"];
                    string TipUcionice = (string)row["TipUcionice"];
                    ucionica.TipUcionice = (enumTipUcionice)Enum.Parse(typeof(enumTipUcionice), TipUcionice);
                    ucionica.IdUstanove = (int)row["IdUstanove"];
                    ucionica.Active = (bool)row["Active"];


                    retVal.Add(ucionica);
                }
            }
            return retVal;
        }

        public Ucionica SelectUcionicaById(int id)
        {
            using (SqlConnection connection = new SqlConnection(ModelHelper.ConnectionString))
            {
                connection.Open();

                DataSet ds = new DataSet();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM UCIONICA WHERE Id = @Id";

                command.Parameters.Add(new SqlParameter("Id", id));


                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;
                dataAdapter.Fill(ds, "Ucionica");

                foreach (DataRow row in ds.Tables["Ucionica"].Rows)
                {
                    Ucionica ucionica = new Ucionica();
                    ucionica.Id = (int)row["Id"];
                    ucionica.BrojUcionice = (string)row["BrojUcionice"];
                    ucionica.BrojMesta = (int)row["BrojMesta"];
                    ucionica.TipUcionice = (enumTipUcionice)row["TipUcionica"];
                    ucionica.IdUstanove = (int)row["IdUstanove"];
                    ucionica.Active = (bool)row["Active"];

                    return ucionica;
                }
            }

            return null;
        }

        public static void DeleteUcionica(Ucionica ucionica)
        {
            using (SqlConnection conn = new SqlConnection(ModelHelper.ConnectionString))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"DELETE FROM UCIONICA WHERE ID=@Id";

                command.Parameters.Add(new SqlParameter("@Id", ucionica.Id));

                command.ExecuteNonQuery();
            }
        }
    }
}
