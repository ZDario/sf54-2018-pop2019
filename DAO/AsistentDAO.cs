﻿using ProjekatPOP.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjekatPOP.DAO
{
    class AsistentDAO
    {
        public static int CreateAsistent(Asistent asistent)
        {
            using (SqlConnection conn = new SqlConnection(ModelHelper.ConnectionString))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"
                    INSERT INTO Asistent (IME, PREZIME, EMAIL, KORISNICKOIME, LOZINKA, TIPKORISNIKA, GODINARODJENJA, PROFESOR, ACTIVE)
                    output inserted.id VALUES (@IME, @PREZIME, @EMAIL, @KORISNICKOIME, @LOZINKA, @TIPKORISNIKA, @GODINARODJENJA, @PROFESOR, @ACTIVE)";

                command.Parameters.Add(new SqlParameter("IME", asistent.Ime));
                command.Parameters.Add(new SqlParameter("PREZIME", asistent.Prezime));
                command.Parameters.Add(new SqlParameter("EMAIL", asistent.Email));
                command.Parameters.Add(new SqlParameter("KORISNICKOIME", asistent.KorisnickoIme));
                command.Parameters.Add(new SqlParameter("LOZINKA", asistent.Lozinka));
                command.Parameters.Add(new SqlParameter("TIPKORISNIKA", asistent.TipKorisnika));
                command.Parameters.Add(new SqlParameter("GODINARODJENJA", asistent.GodinaRodjenja));
                command.Parameters.Add(new SqlParameter("PROFESOR", asistent.IdProfesora));
                command.Parameters.Add(new SqlParameter("ACTIVE", asistent.Active));

                int id = (int)command.ExecuteScalar();
                return id;
            }
        }

        public void UpdateAsistent(Asistent asistent)
        {
            using (SqlConnection sqlConnection = new SqlConnection(ModelHelper.ConnectionString))
            {
                sqlConnection.Open();

                SqlCommand command = sqlConnection.CreateCommand();
                command.CommandText = @"
                        UPDATE Asistent SET
                        IME=@IME, PREZIME=@PREZIME, EMAIL=@EMAIL, KORISNICKOIME=@KORISNICKOIME,
                        LOZINKA=@LOZINKA, TIPKORISNIKA=@TIPKORISNIKA, GODINARODJENJA=@GODINARODJENJA, 
                        PROFESOR=@PROFESOR, ACTIVE=@ACTIVE WHERE ID=@ID";

                command.Parameters.Add(new SqlParameter("IME", asistent.Ime));
                command.Parameters.Add(new SqlParameter("PREZIME", asistent.Prezime));
                command.Parameters.Add(new SqlParameter("EMAIL", asistent.Email));
                command.Parameters.Add(new SqlParameter("KORISNICKOIME", asistent.KorisnickoIme));
                command.Parameters.Add(new SqlParameter("LOZINKA", asistent.Lozinka));
                command.Parameters.Add(new SqlParameter("TIPKORISNIKA", asistent.TipKorisnika.ToString()));
                command.Parameters.Add(new SqlParameter("GODINARODJENJA", asistent.GodinaRodjenja));
                command.Parameters.Add(new SqlParameter("PROFESOR", asistent.IdProfesora));
                command.Parameters.Add(new SqlParameter("ACTIVE", asistent.Active));
                command.Parameters.Add(new SqlParameter("ID", asistent.Id));

                command.ExecuteNonQuery();
            }
        }

        public List<Asistent> SelectAsistent()
        {
            List<Asistent> retVal = new List<Asistent>();

            using (SqlConnection connection = new SqlConnection(ModelHelper.ConnectionString))
            {
                connection.Open();

                DataSet ds = new DataSet();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM ASISTENT";


                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;
                dataAdapter.Fill(ds, "Asistent");


                foreach (DataRow row in ds.Tables["Asistent"].Rows)
                {
                    Asistent asistent = new Asistent();
                    asistent.Id = (int)row["Id"];
                    asistent.Ime = (string)row["Ime"];
                    asistent.Prezime = (string)row["Prezime"];
                    asistent.Email = (string)row["Email"];
                    asistent.KorisnickoIme = (string)row["KorisnickoIme"];
                    asistent.Lozinka = (string)row["Lozinka"];
                    string TipKorisnika = (string)row["TipKorisnika"];
                    asistent.TipKorisnika = (enumTipKorisnika)Enum.Parse(typeof(enumTipKorisnika), TipKorisnika);
                    asistent.GodinaRodjenja = (int)row["GodinaRodjenja"];
                    asistent.IdProfesora = (int)row["Profesor"];
                    asistent.Active = (bool)row["Active"];


                    retVal.Add(asistent);
                }
            }
            return retVal;
        }

        public Asistent SelectAsistentById(int id)
        {
            using (SqlConnection connection = new SqlConnection(ModelHelper.ConnectionString))
            {
                connection.Open();

                DataSet ds = new DataSet();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM ASISTENT WHERE Id = @Id";

                command.Parameters.Add(new SqlParameter("Id", id));


                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;
                dataAdapter.Fill(ds, "Asistent");

                foreach (DataRow row in ds.Tables["Asistent"].Rows)
                {
                    Asistent asistent = new Asistent();
                    asistent.Id = (int)row["Id"];
                    asistent.Ime = (string)row["Ime"];
                    asistent.Prezime = (string)row["Prezime"];
                    asistent.Email = (string)row["Email"];
                    asistent.KorisnickoIme = (string)row["KorisnickoIme"];
                    asistent.Lozinka = (string)row["Lozinka"];
                    asistent.TipKorisnika = (enumTipKorisnika)row["TipKorisnika"];
                    asistent.GodinaRodjenja = (int)row["GodinaRodjenja"];
                    asistent.IdProfesora = (int)row["Profesor"];
                    asistent.Active = (bool)row["Active"];

                    return asistent;
                }
            }
            return null;
        }

        public static void DeleteAsistent(Asistent asistent)
        {
            using (SqlConnection conn = new SqlConnection(ModelHelper.ConnectionString))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"DELETE FROM ASISTENT WHERE ID=@Id";

                command.Parameters.Add(new SqlParameter("@Id", asistent.Id));

                command.ExecuteNonQuery();
            }
        }
    }
}
