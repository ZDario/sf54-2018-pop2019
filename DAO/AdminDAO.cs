﻿using ProjekatPOP.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjekatPOP.DAO
{
    class AdminDAO
    {
        public static int CreateAdmin(Admin admin)
        {
            using (SqlConnection conn = new SqlConnection(ModelHelper.ConnectionString))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"
                    INSERT INTO Admin (IME, PREZIME, EMAIL, KORISNICKOIME, LOZINKA, TIPKORISNIKA, GODINARODJENJA, ACTIVE)
                    output inserted.id VALUES (@IME, @PREZIME, @EMAIL, @KORISNICKOIME, @LOZINKA, @TIPKORISNIKA, @GODINARODJENJA, @ACTIVE)";

                command.Parameters.Add(new SqlParameter("IME", admin.Ime));
                command.Parameters.Add(new SqlParameter("PREZIME", admin.Prezime));
                command.Parameters.Add(new SqlParameter("EMAIL", admin.Email));
                command.Parameters.Add(new SqlParameter("KORISNICKOIME", admin.KorisnickoIme));
                command.Parameters.Add(new SqlParameter("LOZINKA", admin.Lozinka));
                command.Parameters.Add(new SqlParameter("TIPKORISNIKA", admin.TipKorisnika));
                command.Parameters.Add(new SqlParameter("GODINARODJENJA", admin.GodinaRodjenja));
                command.Parameters.Add(new SqlParameter("ACTIVE", admin.Active));

                int id = (int)command.ExecuteScalar();
                return id;
            }
        }

        public void UpdateAdmin(Admin admin)
        {
            using (SqlConnection sqlConnection = new SqlConnection(ModelHelper.ConnectionString))
            {
                sqlConnection.Open();

                SqlCommand command = sqlConnection.CreateCommand();
                command.CommandText = @"
                        UPDATE Admin SET
                        IME=@IME, PREZIME=@PREZIME, EMAIL=@EMAIL, KORISNICKOIME=@KORISNICKOIME,
                        LOZINKA=@LOZINKA, TIPKORISNIKA=@TIPKORISNIKA, GODINARODJENJA=@GODINARODJENJA, 
                        ACTIVE=@ACTIVE WHERE ID=@ID";

                command.Parameters.Add(new SqlParameter("IME", admin.Ime));
                command.Parameters.Add(new SqlParameter("PREZIME", admin.Prezime));
                command.Parameters.Add(new SqlParameter("EMAIL", admin.Email));
                command.Parameters.Add(new SqlParameter("KORISNICKOIME", admin.KorisnickoIme));
                command.Parameters.Add(new SqlParameter("LOZINKA", admin.Lozinka));
                command.Parameters.Add(new SqlParameter("TIPKORISNIKA", admin.TipKorisnika));
                command.Parameters.Add(new SqlParameter("GODINARODJENJA", admin.GodinaRodjenja));
                command.Parameters.Add(new SqlParameter("ACTIVE", admin.Active));
                command.Parameters.Add(new SqlParameter("ID", admin.Id));

                command.ExecuteNonQuery();
            }
        }

        public List<Admin> SelectAdmin()
        {
            List<Admin> retVal = new List<Admin>();

            using (SqlConnection connection = new SqlConnection(ModelHelper.ConnectionString))
            {
                connection.Open();

                DataSet ds = new DataSet();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM ADMIN";


                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;
                dataAdapter.Fill(ds, "Admin");


                foreach (DataRow row in ds.Tables["Admin"].Rows)
                {
                    Admin admin = new Admin();
                    admin.Id = (int)row["Id"];
                    admin.Ime = (string)row["Ime"];
                    admin.Prezime = (string)row["Prezime"];
                    admin.Email = (string)row["Email"];
                    admin.KorisnickoIme = (string)row["KorisnickoIme"];
                    admin.Lozinka = (string)row["Lozinka"];
                    string TipKorisnika = (string)row["TipKorisnika"];
                    admin.TipKorisnika = (enumTipKorisnika)Enum.Parse(typeof(enumTipKorisnika), TipKorisnika);
                    admin.GodinaRodjenja = (int)row["GodinaRodjenja"];
                    admin.Active = (bool)row["Active"];


                    retVal.Add(admin);
                }
            }
            return retVal;
        }

        public Admin SelectAdminById(int id)
        {
            using (SqlConnection connection = new SqlConnection(ModelHelper.ConnectionString))
            {
                connection.Open();

                DataSet ds = new DataSet();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM ADMIN WHERE Id = @Id";

                command.Parameters.Add(new SqlParameter("Id", id));


                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;
                dataAdapter.Fill(ds, "Admin");

                foreach (DataRow row in ds.Tables["Admin"].Rows)
                {
                    Admin admin = new Admin();
                    admin.Id = (int)row["Id"];
                    admin.Ime = (string)row["Ime"];
                    admin.Prezime = (string)row["Prezime"];
                    admin.Email = (string)row["Email"];
                    admin.KorisnickoIme = (string)row["KorisnickoIme"];
                    admin.Lozinka = (string)row["Lozinka"];
                    string TipKorisnika = (string)row["TipKorisnika"];
                    admin.TipKorisnika = (enumTipKorisnika)Enum.Parse(typeof(enumTipKorisnika), TipKorisnika);
                    admin.GodinaRodjenja = (int)row["GodinaRodjenja"];
                    admin.Active = (bool)row["Active"];

                    return admin;
                }
            }
            return null;
        }

        public static void DeleteAdmin(Admin admin)
        {
            using (SqlConnection conn = new SqlConnection(ModelHelper.ConnectionString))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"DELETE FROM ADMIN WHERE ID=@Id";

                command.Parameters.Add(new SqlParameter("@Id", admin.Id));

                command.ExecuteNonQuery();
            }
        }
    }
}
