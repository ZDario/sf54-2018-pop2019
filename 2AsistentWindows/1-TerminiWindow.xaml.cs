﻿using ProjekatPOP.DAO;
using ProjekatPOP.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjekatPOP
{
    /// <summary>
    /// Interaction logic for _1_TerminiWindow.xaml
    /// </summary>
    public partial class _1_TerminiWindow : Window
    {

        public ICollectionView CollectionView;

        public _1_TerminiWindow()
        {
            InitializeComponent();
            CollectionView = CollectionViewSource.GetDefaultView(ModelHelper.GetInstance().Termini);
            CollectionView.Filter = ActiveFilter;
            DataContext = CollectionView;
        }

        public bool ActiveFilter(object obj)
        {

            var term = obj as Termin;
            return term.Active;
        }

        private void AddTerminButton_Click(object sender, RoutedEventArgs e)
        {
            Termin noviTermin = new Termin();
            AddEditTermWindow addEditTermWindow = new AddEditTermWindow(noviTermin);
            addEditTermWindow.ShowDialog();
            bool termAdded = (bool)addEditTermWindow.DialogResult;
            if (termAdded)
            {
                //korisnici.Add(addEditUserWindow.Korisnik);
                ModelHelper.GetInstance().AddTermin(addEditTermWindow.Termin);
                TerminDAO.CreateTermin(addEditTermWindow.Termin);
            }
            CollectionView.Refresh();
        }

        private void DeleteTerminButton_Click(object sender, RoutedEventArgs e)
        {
            var result = MessageBox.Show("Jeste li sigurni da zelite da obrisete?",
                "Brisanje", MessageBoxButton.YesNo);
            if (result == MessageBoxResult.Yes)
            {
                var termin = TerminiGrid.SelectedItem as Termin;
                ModelHelper.GetInstance().DeleteTermin(termin);
                TerminDAO.DeleteTermin(termin);
                CollectionView.Refresh();
            }
            else if (result == MessageBoxResult.No)
            {
                return;
            }
        }

        private void RasporedButton_Click(object sender, RoutedEventArgs e)
        {
            var rasporedWindow = new _2_RasporedWindow();
            rasporedWindow.Show();
        }

        private void ProfilButton_Click(object sender, RoutedEventArgs e)
        {
            var profilWindow = new ProfilWindow();
            profilWindow.Show();
        }
    }
}
