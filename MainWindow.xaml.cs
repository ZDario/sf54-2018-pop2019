﻿using ProjekatPOP;
using ProjekatPOP.Ne_Registrovani;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProjekatPOP
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void NeRegistrovaniButton_Click(object sender, RoutedEventArgs e)
        {
            var neRegistrovani = new _1_InstitutionWindow();
            neRegistrovani.Show();

        }

        private void RegistrovaniButton_Click(object sender, RoutedEventArgs e)
        {
            var registrovaniLogin = new RegistrovaniLogin();
            registrovaniLogin.Show();
        }
    }
}
